<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B06619">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties most gracious speech to both houses of Parliament, on Tuesday the fifth day of July, 1698.</title>
    <author>England and Wales. Sovereign (1694-1702 : William III)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B06619 of text R186679 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing W2416). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B06619</idno>
    <idno type="STC">Wing W2416</idno>
    <idno type="STC">ESTC R186679</idno>
    <idno type="EEBO-CITATION">52529106</idno>
    <idno type="OCLC">ocm 52529106</idno>
    <idno type="VID">179260</idno>
    <idno type="PROQUESTGOID">2240867504</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B06619)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179260)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2781:27)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties most gracious speech to both houses of Parliament, on Tuesday the fifth day of July, 1698.</title>
      <author>England and Wales. Sovereign (1694-1702 : William III)</author>
      <author>William, III, King of England, 1650-1702.</author>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by the Heirs and Successors of Andrew Anderson, Printer to His Most Excellent Majesty,</publisher>
      <pubPlace>Printed at London ;</pubPlace>
      <pubPlace>and reprinted at Edinburgh :</pubPlace>
      <date>1698.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Parliament -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1689-1702 -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>His Majesties most gracious speech to both Houses of Parliament, on Tuesday the fifth day of July, 1698.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1698</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>2</ep:pageCount>
    <ep:wordCount>421</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-02</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-03</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-04</date>
    <label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-10</date>
    <label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change>
    <date>2009-07</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-07</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B06619-e10010">
  <body xml:id="B06619-e10020">
   <div type="speech" xml:id="B06619-e10030">
    <pb facs="tcp:179260:1" xml:id="B06619-001-a"/>
    <pb facs="tcp:179260:1" rend="simple:additions" xml:id="B06619-001-b"/>
    <head xml:id="B06619-e10040">
     <w lemma="his" pos="po" xml:id="B06619-001-b-0010">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="B06619-001-b-0020">Majesties</w>
     <w lemma="most" pos="avs-d" xml:id="B06619-001-b-0030">MOST</w>
     <w lemma="gracious" pos="j" xml:id="B06619-001-b-0040">GRACIOUS</w>
     <w lemma="speech" pos="n1" xml:id="B06619-001-b-0050">SPEECH</w>
     <w lemma="to" pos="acp" xml:id="B06619-001-b-0060">To</w>
     <w lemma="both" pos="d" xml:id="B06619-001-b-0070">both</w>
     <w lemma="house" pos="n2" xml:id="B06619-001-b-0080">Houses</w>
     <w lemma="of" pos="acp" xml:id="B06619-001-b-0090">of</w>
     <w lemma="parliament" pos="n1" xml:id="B06619-001-b-0100">PARLIAMENT</w>
     <pc xml:id="B06619-001-b-0110">,</pc>
     <w lemma="on" pos="acp" xml:id="B06619-001-b-0120">On</w>
     <date xml:id="B06619-e10050">
      <w lemma="Tuesday" pos="nn1" rend="hi" xml:id="B06619-001-b-0130">Tuesday</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-0140">the</w>
      <w lemma="five" pos="ord" xml:id="B06619-001-b-0150">Fifth</w>
      <w lemma="day" pos="n1" xml:id="B06619-001-b-0160">Day</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-0170">of</w>
      <w lemma="July" pos="nn1" rend="hi" xml:id="B06619-001-b-0180">July</w>
      <pc xml:id="B06619-001-b-0190">,</pc>
      <w lemma="1698" pos="crd" xml:id="B06619-001-b-0200">1698</w>
      <pc xml:id="B06619-001-b-0210">,</pc>
     </date>
    </head>
    <q xml:id="B06619-e10080">
     <p xml:id="B06619-e10090">
      <hi xml:id="B06619-e10100">
       <w lemma="my" pos="po" xml:id="B06619-001-b-0220">My</w>
       <w lemma="lord" pos="n2" xml:id="B06619-001-b-0230">Lords</w>
       <w lemma="and" pos="cc" xml:id="B06619-001-b-0240">and</w>
       <w lemma="gentleman" pos="n2" xml:id="B06619-001-b-0250">Gentlemen</w>
      </hi>
      <pc rend="follows-hi" xml:id="B06619-001-b-0260">,</pc>
      <lb xml:id="B06619-e10110"/>
      <w lemma="I" pos="pns" xml:id="B06619-001-b-0270">I</w>
      <w lemma="can" pos="vmbx" xml:id="B06619-001-b-0280">Cannot</w>
      <w lemma="take" pos="vvi" xml:id="B06619-001-b-0290">take</w>
      <w lemma="leave" pos="vvb" xml:id="B06619-001-b-0300">Leave</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-0310">of</w>
      <w lemma="so" pos="av" xml:id="B06619-001-b-0320">so</w>
      <w lemma="good" pos="j" xml:id="B06619-001-b-0330">good</w>
      <w lemma="a" pos="d" xml:id="B06619-001-b-0340">a</w>
      <w lemma="parliament" pos="n1" xml:id="B06619-001-b-0350">Parliament</w>
      <pc xml:id="B06619-001-b-0360">,</pc>
      <w lemma="without" pos="acp" xml:id="B06619-001-b-0370">without</w>
      <w lemma="public" pos="av-j" reg="Publicly" xml:id="B06619-001-b-0380">Publickly</w>
      <w lemma="acknowledge" pos="vvg" xml:id="B06619-001-b-0390">Acknowledging</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-0400">the</w>
      <w lemma="sense" pos="n1" xml:id="B06619-001-b-0410">Sense</w>
      <w lemma="I" pos="pns" xml:id="B06619-001-b-0420">I</w>
      <w lemma="have" pos="vvb" xml:id="B06619-001-b-0430">have</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-0440">of</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-0450">the</w>
      <w lemma="great" pos="j" xml:id="B06619-001-b-0460">Great</w>
      <w lemma="thing" pos="n2" xml:id="B06619-001-b-0470">Things</w>
      <w lemma="you" pos="pn" xml:id="B06619-001-b-0480">you</w>
      <w lemma="have" pos="vvb" xml:id="B06619-001-b-0490">have</w>
      <w lemma="do" pos="vvn" xml:id="B06619-001-b-0500">done</w>
      <w lemma="for" pos="acp" xml:id="B06619-001-b-0510">for</w>
      <w lemma="my" pos="po" xml:id="B06619-001-b-0520">My</w>
      <w lemma="safety" pos="n1" xml:id="B06619-001-b-0530">Safety</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-0540">and</w>
      <w lemma="honour" pos="n1" xml:id="B06619-001-b-0550">Honour</w>
      <pc xml:id="B06619-001-b-0560">,</pc>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-0570">and</w>
      <w lemma="for" pos="acp" xml:id="B06619-001-b-0580">for</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-0590">the</w>
      <w lemma="support" pos="n1" xml:id="B06619-001-b-0600">Support</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-0610">and</w>
      <w lemma="welfare" pos="n1" xml:id="B06619-001-b-0620">Welfare</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-0630">of</w>
      <w lemma="my" pos="po" xml:id="B06619-001-b-0640">My</w>
      <w lemma="people" pos="n1" xml:id="B06619-001-b-0650">People</w>
      <pc unit="sentence" xml:id="B06619-001-b-0660">.</pc>
     </p>
     <p xml:id="B06619-e10120">
      <w lemma="every" pos="d" xml:id="B06619-001-b-0670">Every</w>
      <w lemma="one" pos="pi" xml:id="B06619-001-b-0680">one</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-0690">of</w>
      <w lemma="your" pos="po" xml:id="B06619-001-b-0700">your</w>
      <w lemma="session" pos="n2" xml:id="B06619-001-b-0710">Sessions</w>
      <w lemma="have" pos="vvz" xml:id="B06619-001-b-0720">hath</w>
      <w lemma="make" pos="vvn" xml:id="B06619-001-b-0730">made</w>
      <w lemma="good" pos="j" xml:id="B06619-001-b-0740">good</w>
      <w lemma="this" pos="d" xml:id="B06619-001-b-0750">this</w>
      <w lemma="character" pos="n1" xml:id="B06619-001-b-0760">Character</w>
      <pc xml:id="B06619-001-b-0770">:</pc>
      <w lemma="the" pos="d" xml:id="B06619-001-b-0780">The</w>
      <w lemma="happy" pos="j" xml:id="B06619-001-b-0790">Happy</w>
      <w lemma="unite" pos="n1-vg" xml:id="B06619-001-b-0800">Uniting</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-0810">of</w>
      <w lemma="we" pos="pno" xml:id="B06619-001-b-0820">Us</w>
      <w lemma="in" pos="acp" xml:id="B06619-001-b-0830">in</w>
      <w lemma="a" pos="d" xml:id="B06619-001-b-0840">an</w>
      <w lemma="association" pos="n1" xml:id="B06619-001-b-0850">Association</w>
      <w lemma="for" pos="acp" xml:id="B06619-001-b-0860">for</w>
      <w lemma="our" pos="po" xml:id="B06619-001-b-0870">Our</w>
      <w lemma="mutual" pos="j" xml:id="B06619-001-b-0880">Mutual</w>
      <w lemma="defence" pos="n1" xml:id="B06619-001-b-0890">Defence</w>
      <pc xml:id="B06619-001-b-0900">;</pc>
      <w lemma="the" pos="d" xml:id="B06619-001-b-0910">The</w>
      <w lemma="remedy" pos="vvg" xml:id="B06619-001-b-0920">Remedying</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-0930">the</w>
      <w lemma="corruption" pos="n1" xml:id="B06619-001-b-0940">Corruption</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-0950">of</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-0960">the</w>
      <w lemma="coin" pos="n1" xml:id="B06619-001-b-0970">Coin</w>
      <pc xml:id="B06619-001-b-0980">,</pc>
      <w lemma="which" pos="crq" xml:id="B06619-001-b-0990">which</w>
      <w lemma="have" pos="vvd" xml:id="B06619-001-b-1000">had</w>
      <w lemma="be" pos="vvn" xml:id="B06619-001-b-1010">been</w>
      <w lemma="so" pos="av" xml:id="B06619-001-b-1020">so</w>
      <w lemma="long" pos="av-j" xml:id="B06619-001-b-1030">long</w>
      <w lemma="grow" pos="vvg" xml:id="B06619-001-b-1040">Growing</w>
      <w lemma="upon" pos="acp" xml:id="B06619-001-b-1050">upon</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-1060">the</w>
      <w lemma="nation" pos="n1" xml:id="B06619-001-b-1070">Nation</w>
      <pc xml:id="B06619-001-b-1080">;</pc>
      <w lemma="the" pos="d" xml:id="B06619-001-b-1090">The</w>
      <w lemma="restore" pos="j-vg" xml:id="B06619-001-b-1100">Restoring</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-1110">of</w>
      <w lemma="credit" pos="n1" xml:id="B06619-001-b-1120">Credit</w>
      <pc xml:id="B06619-001-b-1130">;</pc>
      <w lemma="the" pos="d" xml:id="B06619-001-b-1140">The</w>
      <w lemma="give" pos="j-vg" xml:id="B06619-001-b-1150">Giving</w>
      <w lemma="supply" pos="n2" xml:id="B06619-001-b-1160">Supplies</w>
      <w lemma="in" pos="acp" xml:id="B06619-001-b-1170">in</w>
      <w lemma="such" pos="d" xml:id="B06619-001-b-1180">such</w>
      <w lemma="a" pos="d" xml:id="B06619-001-b-1190">a</w>
      <w lemma="manner" pos="n1" xml:id="B06619-001-b-1200">manner</w>
      <w lemma="for" pos="acp" xml:id="B06619-001-b-1210">for</w>
      <w lemma="carry" pos="vvg" xml:id="B06619-001-b-1220">Carrying</w>
      <w lemma="on" pos="acp" xml:id="B06619-001-b-1230">on</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-1240">the</w>
      <w lemma="war" pos="n1" xml:id="B06619-001-b-1250">War</w>
      <pc xml:id="B06619-001-b-1260">,</pc>
      <w lemma="as" pos="acp" xml:id="B06619-001-b-1270">as</w>
      <w lemma="do" pos="vvd" xml:id="B06619-001-b-1280">did</w>
      <pc xml:id="B06619-001-b-1290">,</pc>
      <w lemma="by" pos="acp" xml:id="B06619-001-b-1300">by</w>
      <w lemma="God" pos="nng1" reg="God's" xml:id="B06619-001-b-1310">Gods</w>
      <w lemma="blessing" pos="n1" xml:id="B06619-001-b-1320">Blessing</w>
      <pc xml:id="B06619-001-b-1330">,</pc>
      <w lemma="produce" pos="vvb" xml:id="B06619-001-b-1340">Produce</w>
      <w lemma="a" pos="d" xml:id="B06619-001-b-1350">an</w>
      <w lemma="honourable" pos="j" xml:id="B06619-001-b-1360">Honourable</w>
      <w lemma="peace" pos="n1" xml:id="B06619-001-b-1370">Peace</w>
      <pc xml:id="B06619-001-b-1380">;</pc>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-1390">And</w>
      <w lemma="after" pos="acp" xml:id="B06619-001-b-1400">after</w>
      <w lemma="that" pos="cs" xml:id="B06619-001-b-1410">that</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-1420">the</w>
      <w lemma="make" pos="vvg" xml:id="B06619-001-b-1430">Making</w>
      <w lemma="such" pos="d" xml:id="B06619-001-b-1440">such</w>
      <w lemma="provision" pos="n2" xml:id="B06619-001-b-1450">Provisions</w>
      <w lemma="for" pos="acp" xml:id="B06619-001-b-1460">for</w>
      <w lemma="our" pos="po" xml:id="B06619-001-b-1470">Our</w>
      <w lemma="common" pos="j" xml:id="B06619-001-b-1480">Common</w>
      <w lemma="security" pos="n1" xml:id="B06619-001-b-1490">Security</w>
      <pc xml:id="B06619-001-b-1500">,</pc>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-1510">and</w>
      <w lemma="towards" pos="acp" xml:id="B06619-001-b-1520">towards</w>
      <w lemma="satisfy" pos="vvg" xml:id="B06619-001-b-1530">Satisfying</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-1540">the</w>
      <w lemma="debt" pos="n2" xml:id="B06619-001-b-1550">Debts</w>
      <w lemma="contract" pos="vvn" xml:id="B06619-001-b-1560">Contracted</w>
      <w lemma="in" pos="acp" xml:id="B06619-001-b-1570">in</w>
      <w lemma="so" pos="av" xml:id="B06619-001-b-1580">so</w>
      <w lemma="long" pos="j" xml:id="B06619-001-b-1590">long</w>
      <w lemma="a" pos="d" xml:id="B06619-001-b-1600">a</w>
      <w lemma="war" pos="n1" xml:id="B06619-001-b-1610">War</w>
      <pc xml:id="B06619-001-b-1620">,</pc>
      <w lemma="with" pos="acp" xml:id="B06619-001-b-1630">with</w>
      <w lemma="as" pos="acp" xml:id="B06619-001-b-1640">as</w>
      <w lemma="little" pos="j" xml:id="B06619-001-b-1650">little</w>
      <w lemma="Burden" pos="nn1" xml:id="B06619-001-b-1660">Burden</w>
      <w lemma="to" pos="acp" xml:id="B06619-001-b-1670">to</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-1680">the</w>
      <w lemma="kingdom" pos="n1" xml:id="B06619-001-b-1690">Kingdom</w>
      <w lemma="as" pos="acp" xml:id="B06619-001-b-1700">as</w>
      <w lemma="be" pos="vvz" xml:id="B06619-001-b-1710">is</w>
      <w lemma="possible" pos="j" xml:id="B06619-001-b-1720">possible</w>
      <pc xml:id="B06619-001-b-1730">,</pc>
      <w lemma="be" pos="vvb" xml:id="B06619-001-b-1740">are</w>
      <w lemma="such" pos="d" xml:id="B06619-001-b-1750">such</w>
      <w lemma="thing" pos="n2" xml:id="B06619-001-b-1760">things</w>
      <w lemma="as" pos="acp" xml:id="B06619-001-b-1770">as</w>
      <w lemma="will" pos="vmb" xml:id="B06619-001-b-1780">will</w>
      <w lemma="give" pos="vvi" xml:id="B06619-001-b-1790">give</w>
      <w lemma="a" pos="d" xml:id="B06619-001-b-1800">a</w>
      <w lemma="last" pos="j-vg" xml:id="B06619-001-b-1810">lasting</w>
      <w lemma="reputation" pos="n1" xml:id="B06619-001-b-1820">Reputation</w>
      <w lemma="to" pos="acp" xml:id="B06619-001-b-1830">to</w>
      <w lemma="this" pos="d" xml:id="B06619-001-b-1840">this</w>
      <w lemma="parliament" pos="n1" xml:id="B06619-001-b-1850">Parliament</w>
      <pc xml:id="B06619-001-b-1860">,</pc>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-1870">and</w>
      <w lemma="will" pos="vmb" xml:id="B06619-001-b-1880">will</w>
      <w lemma="be" pos="vvi" xml:id="B06619-001-b-1890">be</w>
      <w lemma="a" pos="d" xml:id="B06619-001-b-1900">a</w>
      <w lemma="subject" pos="j" xml:id="B06619-001-b-1910">Subject</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-1920">of</w>
      <w lemma="emulation" pos="n1" xml:id="B06619-001-b-1930">Emulation</w>
      <w lemma="to" pos="acp" xml:id="B06619-001-b-1940">to</w>
      <w lemma="those" pos="d" xml:id="B06619-001-b-1950">those</w>
      <w lemma="who" pos="crq" xml:id="B06619-001-b-1960">who</w>
      <w lemma="shall" pos="vmb" xml:id="B06619-001-b-1970">shall</w>
      <w lemma="come" pos="vvi" xml:id="B06619-001-b-1980">come</w>
      <w lemma="after" pos="acp" xml:id="B06619-001-b-1990">after</w>
      <pc unit="sentence" xml:id="B06619-001-b-2000">.</pc>
     </p>
     <p xml:id="B06619-e10130">
      <w lemma="beside" pos="acp" xml:id="B06619-001-b-2010">Beside</w>
      <w lemma="all" pos="d" xml:id="B06619-001-b-2020">all</w>
      <w lemma="this" pos="d" xml:id="B06619-001-b-2030">this</w>
      <pc xml:id="B06619-001-b-2040">,</pc>
      <w lemma="I" pos="pns" xml:id="B06619-001-b-2050">I</w>
      <w lemma="think" pos="vvb" xml:id="B06619-001-b-2060">think</w>
      <w lemma="myself" pos="pr" reg="Myself" xml:id="B06619-001-b-2070">My Self</w>
      <w lemma="personal" pos="av-j" xml:id="B06619-001-b-2090">Personally</w>
      <w lemma="oblige" pos="vvn" xml:id="B06619-001-b-2100">Obliged</w>
      <w lemma="to" pos="prt" xml:id="B06619-001-b-2110">to</w>
      <w lemma="return" pos="vvi" xml:id="B06619-001-b-2120">Return</w>
      <w lemma="my" pos="po" xml:id="B06619-001-b-2130">my</w>
      <w lemma="thanks" pos="n2" xml:id="B06619-001-b-2140">Thanks</w>
      <w lemma="to" pos="acp" xml:id="B06619-001-b-2150">to</w>
      <w lemma="you" pos="pn" xml:id="B06619-001-b-2160">you</w>
      <pc xml:id="B06619-001-b-2170">,</pc>
      <hi xml:id="B06619-e10140">
       <w lemma="gentleman" pos="n2" xml:id="B06619-001-b-2180">Gentlemen</w>
       <w lemma="of" pos="acp" xml:id="B06619-001-b-2190">of</w>
       <w lemma="the" pos="d" xml:id="B06619-001-b-2200">the</w>
       <w lemma="house" pos="n1" xml:id="B06619-001-b-2210">House</w>
       <w lemma="of" pos="acp" xml:id="B06619-001-b-2220">of</w>
       <w lemma="commons" pos="n2" xml:id="B06619-001-b-2230">Commons</w>
       <pc xml:id="B06619-001-b-2240">,</pc>
      </hi>
      <w lemma="for" pos="acp" xml:id="B06619-001-b-2250">for</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-2260">the</w>
      <w lemma="regard" pos="vvb" xml:id="B06619-001-b-2270">Regard</w>
      <w lemma="you" pos="pn" xml:id="B06619-001-b-2280">you</w>
      <pc xml:id="B06619-001-b-2290">,</pc>
      <w lemma="have" pos="vvb" xml:id="B06619-001-b-2300">have</w>
      <w lemma="have" pos="vvn" xml:id="B06619-001-b-2310">had</w>
      <w lemma="to" pos="acp" xml:id="B06619-001-b-2320">to</w>
      <w lemma="my" pos="po" xml:id="B06619-001-b-2330">My</w>
      <w lemma="honour" pos="n1" xml:id="B06619-001-b-2340">Honour</w>
      <pc xml:id="B06619-001-b-2350">,</pc>
      <w lemma="by" pos="acp" xml:id="B06619-001-b-2360">by</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-2370">the</w>
      <w lemma="establishment" pos="n1" xml:id="B06619-001-b-2380">Establishment</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-2390">of</w>
      <w lemma="my" pos="po" xml:id="B06619-001-b-2400">my</w>
      <w lemma="revenue" pos="n1" xml:id="B06619-001-b-2410">Revenue</w>
      <pc unit="sentence" xml:id="B06619-001-b-2420">.</pc>
     </p>
     <p xml:id="B06619-e10150">
      <hi xml:id="B06619-e10160">
       <w lemma="my" pos="po" xml:id="B06619-001-b-2430">My</w>
       <w lemma="lord" pos="n2" xml:id="B06619-001-b-2440">Lords</w>
       <w lemma="and" pos="cc" xml:id="B06619-001-b-2450">and</w>
       <w lemma="gentleman" pos="n2" xml:id="B06619-001-b-2460">Gentlemen</w>
      </hi>
      <pc rend="follows-hi" unit="sentence" xml:id="B06619-001-b-2470">.</pc>
      <w lemma="there" pos="av" xml:id="B06619-001-b-2480">There</w>
      <w lemma="be" pos="vvz" xml:id="B06619-001-b-2490">is</w>
      <w lemma="nothing" pos="pix" xml:id="B06619-001-b-2500">nothing</w>
      <w lemma="I" pos="pns" xml:id="B06619-001-b-2510">I</w>
      <w lemma="value" pos="vvi" xml:id="B06619-001-b-2520">Value</w>
      <w lemma="so" pos="av" xml:id="B06619-001-b-2530">so</w>
      <w lemma="much" pos="av-d" xml:id="B06619-001-b-2540">much</w>
      <w lemma="as" pos="acp" xml:id="B06619-001-b-2550">as</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-2560">the</w>
      <w lemma="esteem" pos="n1" xml:id="B06619-001-b-2570">Esteem</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-2580">and</w>
      <w lemma="love" pos="n1" xml:id="B06619-001-b-2590">Love</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-2600">of</w>
      <w lemma="my" pos="po" xml:id="B06619-001-b-2610">My</w>
      <w lemma="people" pos="n1" xml:id="B06619-001-b-2620">People</w>
      <pc xml:id="B06619-001-b-2630">;</pc>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-2640">And</w>
      <w lemma="as" pos="acp" xml:id="B06619-001-b-2650">as</w>
      <w lemma="for" pos="acp" xml:id="B06619-001-b-2660">for</w>
      <w lemma="their" pos="po" xml:id="B06619-001-b-2670">their</w>
      <w lemma="sake" pos="n2" xml:id="B06619-001-b-2680">Sakes</w>
      <pc xml:id="B06619-001-b-2690">,</pc>
      <w lemma="I" pos="pns" rend="hi" xml:id="B06619-001-b-2700">I</w>
      <w lemma="avoid" pos="vvd" xml:id="B06619-001-b-2710">avoided</w>
      <w lemma="no" pos="dx" xml:id="B06619-001-b-2720">no</w>
      <w lemma="hazard" pos="n2" xml:id="B06619-001-b-2730">Hazards</w>
      <w lemma="during" pos="acp" xml:id="B06619-001-b-2740">during</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-2750">the</w>
      <w lemma="war" pos="n1" xml:id="B06619-001-b-2760">War</w>
      <w lemma="so" pos="av" xml:id="B06619-001-b-2770">so</w>
      <w lemma="my" pos="po" xml:id="B06619-001-b-2780">My</w>
      <w lemma="whole" pos="j" xml:id="B06619-001-b-2790">whole</w>
      <w lemma="study" pos="n1" xml:id="B06619-001-b-2800">Study</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-2810">and</w>
      <w lemma="care" pos="n1" xml:id="B06619-001-b-2820">Care</w>
      <w lemma="shall" pos="vmb" xml:id="B06619-001-b-2830">shall</w>
      <w lemma="be" pos="vvi" xml:id="B06619-001-b-2840">be</w>
      <w lemma="to" pos="prt" xml:id="B06619-001-b-2850">to</w>
      <w lemma="improve" pos="vvb" xml:id="B06619-001-b-2860">Improve</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-2870">and</w>
      <w lemma="continue" pos="vvb" xml:id="B06619-001-b-2880">Continue</w>
      <w lemma="to" pos="acp" xml:id="B06619-001-b-2890">to</w>
      <w lemma="they" pos="pno" xml:id="B06619-001-b-2900">them</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-2910">the</w>
      <w lemma="advantage" pos="n2" xml:id="B06619-001-b-2920">Advantages</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-2930">and</w>
      <w lemma="blessing" pos="n2" xml:id="B06619-001-b-2940">Blessings</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-2950">of</w>
      <w lemma="peace" pos="n1" xml:id="B06619-001-b-2960">Peace</w>
      <pc unit="sentence" xml:id="B06619-001-b-2970">.</pc>
     </p>
     <p xml:id="B06619-e10180">
      <w lemma="and" pos="cc" xml:id="B06619-001-b-2980">And</w>
      <w lemma="I" pos="pns" rend="hi" xml:id="B06619-001-b-2990">I</w>
      <w lemma="earnest" pos="av-j" xml:id="B06619-001-b-3000">earnestly</w>
      <w lemma="desire" pos="vvb" xml:id="B06619-001-b-3010">Desire</w>
      <w lemma="you" pos="pn" xml:id="B06619-001-b-3020">you</w>
      <w lemma="all" pos="d" xml:id="B06619-001-b-3030">all</w>
      <pc xml:id="B06619-001-b-3040">,</pc>
      <w lemma="in" pos="acp" xml:id="B06619-001-b-3050">in</w>
      <w lemma="your" pos="po" xml:id="B06619-001-b-3060">your</w>
      <w lemma="several" pos="j" xml:id="B06619-001-b-3070">several</w>
      <w lemma="station" pos="n2" xml:id="B06619-001-b-3080">Stations</w>
      <pc xml:id="B06619-001-b-3090">,</pc>
      <w lemma="to" pos="prt" xml:id="B06619-001-b-3100">to</w>
      <w lemma="be" pos="vvi" xml:id="B06619-001-b-3110">be</w>
      <w lemma="vigilant" pos="j" xml:id="B06619-001-b-3120">Vigilant</w>
      <w lemma="in" pos="acp" xml:id="B06619-001-b-3130">in</w>
      <w lemma="preserve" pos="vvg" xml:id="B06619-001-b-3140">Preserving</w>
      <w lemma="peace" pos="n1" xml:id="B06619-001-b-3150">Peace</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-3160">and</w>
      <w lemma="good" pos="j" xml:id="B06619-001-b-3170">Good</w>
      <w lemma="order" pos="n1" xml:id="B06619-001-b-3180">Order</w>
      <pc xml:id="B06619-001-b-3190">;</pc>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-3200">and</w>
      <w lemma="in" pos="acp" xml:id="B06619-001-b-3210">in</w>
      <w lemma="a" pos="d" xml:id="B06619-001-b-3220">a</w>
      <w lemma="due" pos="j" xml:id="B06619-001-b-3230">Due</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-3240">and</w>
      <w lemma="regular" pos="j" xml:id="B06619-001-b-3250">Regular</w>
      <w lemma="execution" pos="n1" xml:id="B06619-001-b-3260">Execution</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-3270">of</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-3280">the</w>
      <w lemma="law" pos="n2" xml:id="B06619-001-b-3290">Laws</w>
      <pc xml:id="B06619-001-b-3300">,</pc>
      <w lemma="especial" pos="av-j" xml:id="B06619-001-b-3310">especially</w>
      <w lemma="those" pos="d" xml:id="B06619-001-b-3320">those</w>
      <w lemma="against" pos="acp" xml:id="B06619-001-b-3330">against</w>
      <w lemma="profaneness" pos="n1" xml:id="B06619-001-b-3340">Profaneness</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-3350">and</w>
      <w lemma="irreligion" pos="n1" xml:id="B06619-001-b-3360">Irreligion</w>
      <pc unit="sentence" xml:id="B06619-001-b-3370">.</pc>
     </p>
    </q>
    <p xml:id="B06619-e10200">
     <w lemma="then" pos="av" xml:id="B06619-001-b-3380">Then</w>
     <w lemma="the" pos="d" xml:id="B06619-001-b-3390">the</w>
     <w lemma="lord" pos="n1" xml:id="B06619-001-b-3400">Lord</w>
     <w lemma="chancellor" pos="n1" xml:id="B06619-001-b-3410">Chancellor</w>
     <pc xml:id="B06619-001-b-3420">,</pc>
     <w lemma="by" pos="acp" xml:id="B06619-001-b-3430">by</w>
     <w lemma="his" pos="po" xml:id="B06619-001-b-3440">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="B06619-001-b-3450">Majesties</w>
     <w lemma="command" pos="n1" xml:id="B06619-001-b-3460">Command</w>
     <pc xml:id="B06619-001-b-3470">,</pc>
     <w lemma="say" pos="vvn" xml:id="B06619-001-b-3480">said</w>
     <pc xml:id="B06619-001-b-3490">,</pc>
     <lb xml:id="B06619-e10210"/>
     <q xml:id="B06619-e10220">
      <hi xml:id="B06619-e10230">
       <w lemma="my" pos="po" xml:id="B06619-001-b-3500">My</w>
       <w lemma="lord" pos="n2" xml:id="B06619-001-b-3510">Lords</w>
       <w lemma="and" pos="cc" xml:id="B06619-001-b-3520">and</w>
       <w lemma="gentleman" pos="n2" xml:id="B06619-001-b-3530">Gentlemen</w>
      </hi>
      <pc rend="follows-hi" xml:id="B06619-001-b-3540">,</pc>
      <lb xml:id="B06619-e10240"/>
      <w lemma="his" pos="po" xml:id="B06619-001-b-3550">HIS</w>
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="B06619-001-b-3560">Majesties</w>
      <w lemma="royal" pos="j" xml:id="B06619-001-b-3570">Royal</w>
      <w lemma="will" pos="n1" xml:id="B06619-001-b-3580">Will</w>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-3590">and</w>
      <w lemma="pleasure" pos="n1" xml:id="B06619-001-b-3600">Pleasure</w>
      <w lemma="be" pos="vvz" xml:id="B06619-001-b-3610">is</w>
      <pc xml:id="B06619-001-b-3620">,</pc>
      <w lemma="that" pos="cs" xml:id="B06619-001-b-3630">That</w>
      <w lemma="this" pos="d" xml:id="B06619-001-b-3640">this</w>
      <w lemma="parliament" pos="n1" xml:id="B06619-001-b-3650">Parliament</w>
      <w lemma="be" pos="vvi" xml:id="B06619-001-b-3660">be</w>
      <w lemma="prorogue" pos="vvn" xml:id="B06619-001-b-3670">Prorogued</w>
      <w lemma="to" pos="acp" xml:id="B06619-001-b-3680">to</w>
      <w lemma="Tuesday" pos="nn1" rend="hi" xml:id="B06619-001-b-3690">Tuesday</w>
      <w lemma="the" pos="d" xml:id="B06619-001-b-3700">the</w>
      <w lemma="second" pos="ord" xml:id="B06619-001-b-3710">second</w>
      <w lemma="day" pos="n1" xml:id="B06619-001-b-3720">Day</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-3730">of</w>
      <w lemma="August" pos="nn1" rend="hi" xml:id="B06619-001-b-3740">August</w>
      <w lemma="next" pos="ord" xml:id="B06619-001-b-3750">next</w>
      <pc xml:id="B06619-001-b-3760">,</pc>
      <w lemma="and" pos="cc" xml:id="B06619-001-b-3770">and</w>
      <w lemma="this" pos="d" xml:id="B06619-001-b-3780">this</w>
      <w lemma="parliament" pos="n1" xml:id="B06619-001-b-3790">Parliament</w>
      <w lemma="be" pos="vvz" xml:id="B06619-001-b-3800">is</w>
      <w lemma="prorogue" pos="vvn" xml:id="B06619-001-b-3810">Prorogued</w>
      <w lemma="according" pos="av-j" xml:id="B06619-001-b-3820">accordingly</w>
      <w lemma="to" pos="acp" xml:id="B06619-001-b-3830">to</w>
      <w lemma="Tuesday" pos="nn1" rend="hi" xml:id="B06619-001-b-3840">Tuesday</w>
      <pc xml:id="B06619-001-b-3850">,</pc>
      <w lemma="the" pos="d" xml:id="B06619-001-b-3860">the</w>
      <w lemma="second" pos="ord" xml:id="B06619-001-b-3870">Second</w>
      <w lemma="day" pos="n1" xml:id="B06619-001-b-3880">Day</w>
      <w lemma="of" pos="acp" xml:id="B06619-001-b-3890">of</w>
      <w lemma="August" pos="nn1" rend="hi" xml:id="B06619-001-b-3900">August</w>
      <w lemma="next" pos="ord" xml:id="B06619-001-b-3910">next</w>
      <pc unit="sentence" xml:id="B06619-001-b-3920">.</pc>
     </q>
    </p>
    <trailer xml:id="B06619-e10290">
     <w lemma="n/a" pos="fla" xml:id="B06619-001-b-3930">FINIS</w>
     <pc unit="sentence" xml:id="B06619-001-b-3940">.</pc>
    </trailer>
   </div>
  </body>
  <back xml:id="B06619-e10300">
   <div type="colophon" xml:id="B06619-e10310">
    <p xml:id="B06619-e10320">
     <w lemma="print" pos="j-vn" xml:id="B06619-001-b-3950">Printed</w>
     <w lemma="at" pos="acp" xml:id="B06619-001-b-3960">at</w>
     <w lemma="London" pos="nn1" rend="hi" xml:id="B06619-001-b-3970">London</w>
     <pc xml:id="B06619-001-b-3980">,</pc>
     <w lemma="and" pos="cc" xml:id="B06619-001-b-3990">and</w>
     <w lemma="Re" pos="nn1" xml:id="B06619-001-b-4000">Re</w>
     <w lemma="print" pos="vvn" xml:id="B06619-001-b-4010">printed</w>
     <w lemma="at" pos="acp" xml:id="B06619-001-b-4020">at</w>
     <w lemma="Edinburgh" pos="nn1" rend="hi" xml:id="B06619-001-b-4030">Edinburgh</w>
     <pc xml:id="B06619-001-b-4040">,</pc>
     <w lemma="by" pos="acp" xml:id="B06619-001-b-4050">By</w>
     <w lemma="the" pos="d" xml:id="B06619-001-b-4060">the</w>
     <w lemma="heir" pos="n2" xml:id="B06619-001-b-4070">Heirs</w>
     <w lemma="and" pos="cc" xml:id="B06619-001-b-4080">and</w>
     <w lemma="successor" pos="n2" xml:id="B06619-001-b-4090">Successors</w>
     <w lemma="of" pos="acp" xml:id="B06619-001-b-4100">of</w>
     <hi xml:id="B06619-e10350">
      <w lemma="Andrew" pos="nn1" xml:id="B06619-001-b-4110">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B06619-001-b-4120">Anderson</w>
     </hi>
     <pc rend="follows-hi" xml:id="B06619-001-b-4130">,</pc>
     <w lemma="printer" pos="n1" xml:id="B06619-001-b-4140">Printer</w>
     <w lemma="to" pos="acp" xml:id="B06619-001-b-4150">to</w>
     <w lemma="his" pos="po" xml:id="B06619-001-b-4160">His</w>
     <w lemma="most" pos="avs-d" xml:id="B06619-001-b-4170">Most</w>
     <w lemma="excellent" pos="j" xml:id="B06619-001-b-4180">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="B06619-001-b-4190">Majesty</w>
     <pc unit="sentence" xml:id="B06619-001-b-4200">.</pc>
     <w lemma="1698." pos="crd" xml:id="B06619-001-b-4210">1698.</w>
     <pc unit="sentence" xml:id="B06619-001-b-4220"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
