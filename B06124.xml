<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B06124">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the Kings most excellent Majesty. The humble petition of the governor, assistants, and fellowship of Merchants-Adventurers of England.</title>
    <author>Company of Merchant Adventurers of England.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B06124 of text R185317 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing T1537). Textual changes  and metadata enrichments aim at making the text more  computationally tractable, easier to read, and suitable for network-based collaborative curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in a standardized format that preserves archaic forms ('loveth', 'seekest'). Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2017</date>
    <idno type="DLPS">B06124</idno>
    <idno type="STC">Wing T1537</idno>
    <idno type="STC">ESTC R185317</idno>
    <idno type="EEBO-CITATION">52612447</idno>
    <idno type="OCLC">ocm 52612447</idno>
    <idno type="VID">179668</idno>
    <idno type="PROQUESTGOID">2240886347</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B06124)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179668)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2796:6)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the Kings most excellent Majesty. The humble petition of the governor, assistants, and fellowship of Merchants-Adventurers of England.</title>
      <author>Company of Merchant Adventurers of England.</author>
      <author>Nicholas, Edward, Sir, 1593-1669.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>by Leonard Lichfield,</publisher>
      <pubPlace>Printed at Oxford :</pubPlace>
      <date>1643.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Initial letters.</note>
      <note>Includes the reply in behalf of the King, dated: At the court at Oxford, 26. Martii, 1643, and signed: Edw. Nicholas.</note>
      <note>Reproduction of the original in the Bodleian Library.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Company of Merchant Adventurers of England.</term>
     <term>Great Britain -- Commerce -- History -- 17th century -- Sources.</term>
     <term>Broadsides -- England -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:tcp>B06124</ep:tcp>
    <ep:estc> R185317</ep:estc>
    <ep:stc> (Wing T1537). </ep:stc>
    <ep:corpus>civilwar</ep:corpus>
    <ep:workpart>no</ep:workpart>
    <ep:title>To the Kings most Excellent Majesty. The humble petition of the governor, assistants, and Fellowship of merchants-adventurers of England</ep:title>
    <ep:author>Row, Henry, Sir</ep:author>
    <ep:publicationYear>1643</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>404</ep:wordCount>
    <ep:defectiveTokenCount>2</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>50</ep:defectRate>
    <ep:finalGrade>D</ep:finalGrade>
    <ep:defectRangePerGrade> The  rate of 50 defects per 10,000 words puts this text in the D category of texts with between 35 and 100 defects per 10,000 words.</ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-01</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-02</date>
    <label>Elspeth Healey</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-09</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B06124-e10">
  <body xml:id="B06124-e20">
   <pb facs="tcp:179668:1" rend="simple:additions" xml:id="B06124-001-a"/>
   <div type="petition" xml:id="B06124-e30">
    <head xml:id="B06124-e40">
     <w lemma="to" pos="acp" xml:id="B06124-001-a-0010">To</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-0020">the</w>
     <w lemma="king" pos="ng1" reg="King's" xml:id="B06124-001-a-0030">KINGS</w>
     <w lemma="most" pos="avs-d" xml:id="B06124-001-a-0040">most</w>
     <w lemma="excellent" pos="j" xml:id="B06124-001-a-0050">Excellent</w>
     <hi xml:id="B06124-e50">
      <w lemma="majesty" pos="n1" xml:id="B06124-001-a-0060">MAJESTY</w>
      <pc unit="sentence" xml:id="B06124-001-a-0070">.</pc>
     </hi>
    </head>
    <head type="sub" xml:id="B06124-e60">
     <w lemma="the" pos="d" xml:id="B06124-001-a-0080">The</w>
     <w lemma="humble" pos="j" xml:id="B06124-001-a-0090">humble</w>
     <w lemma="petition" pos="n1" xml:id="B06124-001-a-0100">PETITION</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-0110">of</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-0120">the</w>
     <w lemma="governor" pos="n1" xml:id="B06124-001-a-0130">Governor</w>
     <pc xml:id="B06124-001-a-0140">,</pc>
     <w lemma="assistant" pos="n2" xml:id="B06124-001-a-0150">Assistants</w>
     <pc xml:id="B06124-001-a-0160">,</pc>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-0170">and</w>
     <w lemma="fellowship" pos="n1" xml:id="B06124-001-a-0180">Fellowship</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-0190">of</w>
     <w lemma="merchants-adventurer" pos="n2" xml:id="B06124-001-a-0200">Merchants-Adventurers</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-0210">of</w>
     <w lemma="England" pos="nn1" xml:id="B06124-001-a-0220">England</w>
     <pc unit="sentence" xml:id="B06124-001-a-0230">.</pc>
    </head>
    <p xml:id="B06124-e70">
     <w lemma="most" pos="avs-d" rend="hi" xml:id="B06124-001-a-0240">MOST</w>
     <w lemma="humble" pos="av-j" xml:id="B06124-001-a-0250">humbly</w>
     <w lemma="show" pos="vvz" reg="showeth" xml:id="B06124-001-a-0260">sheweth</w>
     <pc xml:id="B06124-001-a-0270">,</pc>
     <w lemma="that" pos="cs" xml:id="B06124-001-a-0280">That</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-0290">the</w>
     <w lemma="petitioner" pos="n2" xml:id="B06124-001-a-0300">Petitioners</w>
     <w lemma="be" pos="vvg" xml:id="B06124-001-a-0310">being</w>
     <pc xml:id="B06124-001-a-0320">,</pc>
     <w lemma="during" pos="acp" xml:id="B06124-001-a-0330">during</w>
     <w lemma="these" pos="d" xml:id="B06124-001-a-0340">these</w>
     <w lemma="misrable" pos="j" xml:id="B06124-001-a-0350">misrable</w>
     <w lemma="distraction" pos="n2" xml:id="B06124-001-a-0360">distractions</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-0370">of</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-0380">the</w>
     <w lemma="kingdom" pos="n1" reg="kingdom" xml:id="B06124-001-a-0390">Kingdome</w>
     <pc xml:id="B06124-001-a-0400">,</pc>
     <w lemma="encompass" pos="vvn" xml:id="B06124-001-a-0410">encompassed</w>
     <w lemma="on" pos="acp" xml:id="B06124-001-a-0420">on</w>
     <w lemma="all" pos="d" xml:id="B06124-001-a-0430">all</w>
     <w lemma="side" pos="n2" xml:id="B06124-001-a-0440">sides</w>
     <w lemma="with" pos="acp" xml:id="B06124-001-a-0450">with</w>
     <w lemma="many" pos="d" xml:id="B06124-001-a-0460">many</w>
     <w lemma="difficulty" pos="n2" xml:id="B06124-001-a-0470">difficulties</w>
     <pc xml:id="B06124-001-a-0480">;</pc>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-0490">and</w>
     <w lemma="well" pos="av" xml:id="B06124-001-a-0500">well</w>
     <w lemma="weigh" pos="vvg" xml:id="B06124-001-a-0510">weighing</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-0520">the</w>
     <w lemma="danger" pos="n2" xml:id="B06124-001-a-0530">dangers</w>
     <w lemma="they" pos="pns" xml:id="B06124-001-a-0540">they</w>
     <w lemma="may" pos="vmb" xml:id="B06124-001-a-0550">may</w>
     <w lemma="fall" pos="vvi" xml:id="B06124-001-a-0560">fall</w>
     <w lemma="into" pos="acp" xml:id="B06124-001-a-0570">into</w>
     <w lemma="inforraigne" pos="j" xml:id="B06124-001-a-0580">inforraigne</w>
     <w lemma="part" pos="n2" xml:id="B06124-001-a-0590">parts</w>
     <pc xml:id="B06124-001-a-0600">,</pc>
     <w lemma="if" pos="cs" xml:id="B06124-001-a-0610">if</w>
     <w lemma="your" pos="po" xml:id="B06124-001-a-0620">Your</w>
     <w lemma="majesty" pos="n1" rend="hi" xml:id="B06124-001-a-0630">Majesty</w>
     <w lemma="shall" pos="vmd" xml:id="B06124-001-a-0640">should</w>
     <w lemma="withdraw" pos="vvi" xml:id="B06124-001-a-0650">withdraw</w>
     <w lemma="your" pos="po" xml:id="B06124-001-a-0660">Your</w>
     <w lemma="royal" pos="j" reg="royal" xml:id="B06124-001-a-0670">Royall</w>
     <w lemma="protection" pos="n1" xml:id="B06124-001-a-0680">Protection</w>
     <w lemma="from" pos="acp" xml:id="B06124-001-a-0690">from</w>
     <w lemma="they" pos="pno" xml:id="B06124-001-a-0700">them</w>
     <pc xml:id="B06124-001-a-0710">,</pc>
     <w lemma="upon" pos="acp" xml:id="B06124-001-a-0720">upon</w>
     <w lemma="any" pos="d" xml:id="B06124-001-a-0730">any</w>
     <w lemma="displeasure" pos="n1" xml:id="B06124-001-a-0740">displeasure</w>
     <w lemma="to" pos="acp" xml:id="B06124-001-a-0750">to</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-0760">the</w>
     <w lemma="company" pos="n1" xml:id="B06124-001-a-0770">Company</w>
     <pc unit="sentence" xml:id="B06124-001-a-0780">.</pc>
    </p>
    <p xml:id="B06124-e100">
     <w lemma="do" pos="vvb" reg="Do" xml:id="B06124-001-a-0790">Doe</w>
     <w lemma="humble" pos="av-j" xml:id="B06124-001-a-0800">humbly</w>
     <w lemma="prostrate" pos="vvi" xml:id="B06124-001-a-0810">Prostrate</w>
     <w lemma="themselves" pos="pr" xml:id="B06124-001-a-0820">themselves</w>
     <w lemma="before" pos="acp" xml:id="B06124-001-a-0830">before</w>
     <w lemma="your" pos="po" xml:id="B06124-001-a-0840">Your</w>
     <w lemma="royal" pos="j" reg="royal" xml:id="B06124-001-a-0850">Royall</w>
     <hi xml:id="B06124-e110">
      <w lemma="majesty" pos="n1" xml:id="B06124-001-a-0860">Majesty</w>
      <pc xml:id="B06124-001-a-0870">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-0880">and</w>
     <w lemma="for" pos="acp" xml:id="B06124-001-a-0890">for</w>
     <w lemma="their" pos="po" xml:id="B06124-001-a-0900">their</w>
     <w lemma="assurance" pos="n1" xml:id="B06124-001-a-0910">Assurance</w>
     <pc xml:id="B06124-001-a-0920">,</pc>
     <w lemma="in" pos="acp" xml:id="B06124-001-a-0930">in</w>
     <w lemma="so" pos="av" xml:id="B06124-001-a-0940">so</w>
     <w lemma="many" pos="d" xml:id="B06124-001-a-0950">many</w>
     <w lemma="faare" pos="n2" xml:id="B06124-001-a-0960">faares</w>
     <pc xml:id="B06124-001-a-0970">,</pc>
     <w lemma="humble" pos="av-j" xml:id="B06124-001-a-0980">humbly</w>
     <w lemma="beseech" pos="vvb" xml:id="B06124-001-a-0990">beseech</w>
     <w lemma="your" pos="po" xml:id="B06124-001-a-1000">Your</w>
     <w lemma="majesty" pos="n1" rend="hi" xml:id="B06124-001-a-1010">Majesty</w>
     <w lemma="to" pos="prt" xml:id="B06124-001-a-1020">to</w>
     <w lemma="grant" pos="vvi" xml:id="B06124-001-a-1030">grant</w>
     <w lemma="they" pos="pno" xml:id="B06124-001-a-1040">them</w>
     <w lemma="some" pos="d" xml:id="B06124-001-a-1050">some</w>
     <w lemma="particular" pos="j" xml:id="B06124-001-a-1060">particular</w>
     <w lemma="declaration" pos="n1" xml:id="B06124-001-a-1070">Declaration</w>
     <pc xml:id="B06124-001-a-1080">,</pc>
     <w lemma="at" pos="acp" xml:id="B06124-001-a-1090">at</w>
     <w lemma="present" pos="j" xml:id="B06124-001-a-1100">present</w>
     <pc xml:id="B06124-001-a-1110">,</pc>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-1120">of</w>
     <w lemma="your" pos="po" xml:id="B06124-001-a-1130">Your</w>
     <w lemma="royal" pos="j" reg="royal" xml:id="B06124-001-a-1140">Royall</w>
     <w lemma="grace" pos="n1" xml:id="B06124-001-a-1150">Grace</w>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-1160">and</w>
     <w lemma="goodness" pos="n1" xml:id="B06124-001-a-1170">Goodness</w>
     <w lemma="towards" pos="acp" xml:id="B06124-001-a-1180">towards</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-1190">the</w>
     <w lemma="fellowship" pos="n1" xml:id="B06124-001-a-1200">Fellowship</w>
     <pc xml:id="B06124-001-a-1210">,</pc>
     <w lemma="whereby" pos="crq" xml:id="B06124-001-a-1220">whereby</w>
     <w lemma="they" pos="pns" xml:id="B06124-001-a-1230">they</w>
     <w lemma="may" pos="vmb" xml:id="B06124-001-a-1240">may</w>
     <w lemma="be" pos="vvi" xml:id="B06124-001-a-1250">be</w>
     <w lemma="enable" pos="vvn" xml:id="B06124-001-a-1260">enabled</w>
     <w lemma="to" pos="prt" xml:id="B06124-001-a-1270">to</w>
     <w lemma="uphold" pos="vvi" xml:id="B06124-001-a-1280">uphold</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-1290">the</w>
     <w lemma="little" pos="j" xml:id="B06124-001-a-1300">little</w>
     <w lemma="remainder" pos="n1" xml:id="B06124-001-a-1310">remainder</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-1320">of</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-1330">the</w>
     <hi xml:id="B06124-e130">
      <w lemma="staple" pos="n1" xml:id="B06124-001-a-1340">Staple</w>
      <w lemma="trade" pos="n1" xml:id="B06124-001-a-1350">Trade</w>
     </hi>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-1360">of</w>
     <w lemma="this" pos="d" xml:id="B06124-001-a-1370">this</w>
     <hi xml:id="B06124-e140">
      <w lemma="kingdom" pos="n1" xml:id="B06124-001-a-1380">Kingdom</w>
      <pc xml:id="B06124-001-a-1390">,</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-1400">and</w>
     <w lemma="proceed" pos="vvi" xml:id="B06124-001-a-1410">proceed</w>
     <w lemma="in" pos="acp" xml:id="B06124-001-a-1420">in</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-1430">the</w>
     <w lemma="same" pos="d" xml:id="B06124-001-a-1440">same</w>
     <w lemma="with" pos="acp" xml:id="B06124-001-a-1450">with</w>
     <w lemma="encouragement" pos="n1" xml:id="B06124-001-a-1460">encouragement</w>
     <pc xml:id="B06124-001-a-1470">,</pc>
     <w lemma="as" pos="acp" xml:id="B06124-001-a-1480">as</w>
     <w lemma="your" pos="po" xml:id="B06124-001-a-1490">Your</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" rend="hi" xml:id="B06124-001-a-1500">Majesties</w>
     <w lemma="true" pos="j" xml:id="B06124-001-a-1510">true</w>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-1520">and</w>
     <w lemma="loyal" pos="j" reg="loyal" xml:id="B06124-001-a-1530">Loyall</w>
     <w lemma="subject" pos="n2" xml:id="B06124-001-a-1540">Subjects</w>
     <pc unit="sentence" xml:id="B06124-001-a-1550">.</pc>
    </p>
    <closer xml:id="B06124-e160">
     <signed xml:id="B06124-e170">
      <w lemma="and" pos="cc" xml:id="B06124-001-a-1560">And</w>
      <w lemma="as" pos="acp" xml:id="B06124-001-a-1570">as</w>
      <w lemma="in" pos="acp" xml:id="B06124-001-a-1580">in</w>
      <w lemma="duty" pos="n1" xml:id="B06124-001-a-1590">duty</w>
      <w lemma="bind" pos="vvn" xml:id="B06124-001-a-1600">bound</w>
      <pc xml:id="B06124-001-a-1610">,</pc>
      <w lemma="they" pos="pns" xml:id="B06124-001-a-1620">they</w>
      <w lemma="shall" pos="vmb" xml:id="B06124-001-a-1630">shall</w>
      <w lemma="daily" pos="av-j" xml:id="B06124-001-a-1640">daily</w>
      <w lemma="pray" pos="vvi" xml:id="B06124-001-a-1650">Pray</w>
      <pc xml:id="B06124-001-a-1660">,</pc>
      <w lemma="etc." pos="ab" xml:id="B06124-001-a-1670">&amp;c.</w>
      <pc unit="sentence" xml:id="B06124-001-a-1680"/>
      <w lemma="sign" pos="j-vn" xml:id="B06124-001-a-1690">Signed</w>
      <w lemma="by" pos="acp" xml:id="B06124-001-a-1700">by</w>
      <w lemma="sir" pos="n1" xml:id="B06124-001-a-1710">Sir</w>
      <hi xml:id="B06124-e180">
       <w lemma="h" pos="sy" xml:id="B06124-001-a-1720">H</w>
       <w lemma="〈…〉" pos="zz" xml:id="B06124-001-a-1730">〈…〉</w>
       <w lemma="row" pos="n1" xml:id="B06124-001-a-1740">Row</w>
       <pc xml:id="B06124-001-a-1750">,</pc>
      </hi>
      <w lemma="go●●ernor" pos="n1" xml:id="B06124-001-a-1760">Go●●ernor</w>
      <pc unit="sentence" xml:id="B06124-001-a-1770">.</pc>
     </signed>
    </closer>
   </div>
   <div type="reply_to_petition" xml:id="B06124-e190">
    <opener xml:id="B06124-e200">
     <dateline xml:id="B06124-e210">
      <w lemma="at" pos="acp" xml:id="B06124-001-a-1780">At</w>
      <w lemma="the" pos="d" xml:id="B06124-001-a-1790">the</w>
      <w lemma="court" pos="n1" xml:id="B06124-001-a-1800">Court</w>
      <w lemma="at" pos="acp" xml:id="B06124-001-a-1810">at</w>
      <hi xml:id="B06124-e220">
       <w lemma="Oxford" pos="nn1" xml:id="B06124-001-a-1820">Oxford</w>
       <pc xml:id="B06124-001-a-1830">,</pc>
      </hi>
      <date xml:id="B06124-e230">
       <hi xml:id="B06124-e240">
        <w lemma="26." pos="crd" xml:id="B06124-001-a-1840">26.</w>
        <pc unit="sentence" xml:id="B06124-001-a-1850"/>
        <w lemma="march" pos="nn1" xml:id="B06124-001-a-1860">March</w>
       </hi>
       <unclear xml:id="B06124-e250">
        <w lemma="1643" pos="crd" rend="hi" xml:id="B06124-001-a-1870">1643</w>
       </unclear>
      </date>
     </dateline>
    </opener>
    <p xml:id="B06124-e270">
     <w lemma="his" pos="po" rend="decorinit" xml:id="B06124-001-a-1880">HIs</w>
     <w lemma="majesty" pos="n1" xml:id="B06124-001-a-1890">Majesty</w>
     <w lemma="have" pos="vvz" xml:id="B06124-001-a-1900">hath</w>
     <w lemma="command" pos="vvn" xml:id="B06124-001-a-1910">commanded</w>
     <w lemma="i" pos="pno" xml:id="B06124-001-a-1920">me</w>
     <w lemma="to" pos="prt" xml:id="B06124-001-a-1930">to</w>
     <w lemma="give" pos="vvi" xml:id="B06124-001-a-1940">give</w>
     <w lemma="this" pos="d" xml:id="B06124-001-a-1950">this</w>
     <w lemma="his" pos="po" xml:id="B06124-001-a-1960">His</w>
     <w lemma="answer" pos="n1" xml:id="B06124-001-a-1970">Answer</w>
     <pc unit="sentence" xml:id="B06124-001-a-1980">.</pc>
     <w lemma="that" pos="cs" xml:id="B06124-001-a-1990">That</w>
     <w lemma="if" pos="cs" xml:id="B06124-001-a-2000">if</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-2010">the</w>
     <w lemma="petitioner" pos="n2" xml:id="B06124-001-a-2020">Petitioners</w>
     <w lemma="be" pos="vvb" xml:id="B06124-001-a-2030">are</w>
     <w lemma="not" pos="xx" xml:id="B06124-001-a-2040">not</w>
     <w lemma="conscious" pos="j" xml:id="B06124-001-a-2050">conscious</w>
     <w lemma="to" pos="acp" xml:id="B06124-001-a-2060">to</w>
     <w lemma="themselves" pos="pr" xml:id="B06124-001-a-2070">themselves</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-2080">of</w>
     <w lemma="any" pos="d" xml:id="B06124-001-a-2090">any</w>
     <w lemma="disloyalty" pos="n1" xml:id="B06124-001-a-2100">disloyalty</w>
     <w lemma="to" pos="acp" xml:id="B06124-001-a-2110">to</w>
     <w lemma="his" pos="po" xml:id="B06124-001-a-2120">His</w>
     <w lemma="majesty" pos="n1" xml:id="B06124-001-a-2130">Majesty</w>
     <pc xml:id="B06124-001-a-2140">,</pc>
     <w lemma="they" pos="pns" xml:id="B06124-001-a-2150">they</w>
     <w lemma="have" pos="vvb" xml:id="B06124-001-a-2160">have</w>
     <w lemma="no" pos="dx" xml:id="B06124-001-a-2170">no</w>
     <w lemma="reason" pos="n1" xml:id="B06124-001-a-2180">reason</w>
     <w lemma="to" pos="prt" xml:id="B06124-001-a-2190">to</w>
     <w lemma="fear" pos="vvi" reg="fear" xml:id="B06124-001-a-2200">feare</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-2210">the</w>
     <w lemma="withdraw" pos="n1-vg" xml:id="B06124-001-a-2220">withdrawing</w>
     <w lemma="his" pos="po" xml:id="B06124-001-a-2230">His</w>
     <w lemma="royal" pos="j" reg="royal" xml:id="B06124-001-a-2240">Royall</w>
     <w lemma="protection" pos="n1" xml:id="B06124-001-a-2250">Protection</w>
     <w lemma="from" pos="acp" xml:id="B06124-001-a-2260">from</w>
     <w lemma="they" pos="pno" xml:id="B06124-001-a-2270">them</w>
     <pc unit="sentence" xml:id="B06124-001-a-2280">.</pc>
     <w lemma="his" pos="po" xml:id="B06124-001-a-2290">His</w>
     <w lemma="majesty" pos="n1" xml:id="B06124-001-a-2300">Majesty</w>
     <w lemma="be" pos="vvg" xml:id="B06124-001-a-2310">being</w>
     <w lemma="so" pos="av" xml:id="B06124-001-a-2320">so</w>
     <w lemma="desirous" pos="j" xml:id="B06124-001-a-2330">desirous</w>
     <w lemma="to" pos="prt" xml:id="B06124-001-a-2340">to</w>
     <w lemma="preserve" pos="vvi" xml:id="B06124-001-a-2350">preserve</w>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-2360">and</w>
     <w lemma="advance" pos="vvi" xml:id="B06124-001-a-2370">advance</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-2380">the</w>
     <w lemma="public" pos="j" reg="public" xml:id="B06124-001-a-2390">publique</w>
     <w lemma="trade" pos="n1" xml:id="B06124-001-a-2400">Trade</w>
     <pc join="right" xml:id="B06124-001-a-2410">(</pc>
     <w lemma="a" pos="d" xml:id="B06124-001-a-2420">a</w>
     <w lemma="thing" pos="n1" xml:id="B06124-001-a-2430">thing</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-2440">of</w>
     <w lemma="so" pos="av" xml:id="B06124-001-a-2450">so</w>
     <w lemma="high" pos="j" xml:id="B06124-001-a-2460">high</w>
     <w lemma="concernment" pos="n1" xml:id="B06124-001-a-2470">concernment</w>
     <w lemma="to" pos="acp" xml:id="B06124-001-a-2480">to</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-2490">the</w>
     <w lemma="whole" pos="j" xml:id="B06124-001-a-2500">whole</w>
     <w lemma="kingdom" pos="n1" xml:id="B06124-001-a-2510">Kingdom</w>
     <pc xml:id="B06124-001-a-2520">)</pc>
     <w lemma="that" pos="cs" xml:id="B06124-001-a-2530">that</w>
     <w lemma="he" pos="pns" xml:id="B06124-001-a-2540">He</w>
     <w lemma="have" pos="vvz" xml:id="B06124-001-a-2550">hath</w>
     <w lemma="leave" pos="vvn" xml:id="B06124-001-a-2560">left</w>
     <w lemma="no" pos="dx" xml:id="B06124-001-a-2570">no</w>
     <w lemma="mean" pos="n2" xml:id="B06124-001-a-2580">means</w>
     <w lemma="unattempted" pos="j" xml:id="B06124-001-a-2590">unattempted</w>
     <w lemma="to" pos="acp" xml:id="B06124-001-a-2600">to</w>
     <w lemma="that" pos="d" xml:id="B06124-001-a-2610">that</w>
     <w lemma="end" pos="n1" xml:id="B06124-001-a-2620">end</w>
     <pc unit="sentence" xml:id="B06124-001-a-2630">.</pc>
     <w lemma="but" pos="acp" xml:id="B06124-001-a-2640">But</w>
     <w lemma="if" pos="cs" xml:id="B06124-001-a-2650">if</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-2660">the</w>
     <w lemma="petitioner" pos="n2" xml:id="B06124-001-a-2670">Petitioners</w>
     <pc xml:id="B06124-001-a-2680">,</pc>
     <w lemma="or" pos="cc" xml:id="B06124-001-a-2690">or</w>
     <w lemma="any" pos="d" xml:id="B06124-001-a-2700">any</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-2710">of</w>
     <w lemma="they" pos="pno" xml:id="B06124-001-a-2720">them</w>
     <pc xml:id="B06124-001-a-2730">,</pc>
     <w lemma="be" pos="vvb" xml:id="B06124-001-a-2740">are</w>
     <w lemma="fall" pos="vvn" reg="fall'n" xml:id="B06124-001-a-2750">falne</w>
     <w lemma="from" pos="acp" xml:id="B06124-001-a-2760">from</w>
     <w lemma="their" pos="po" xml:id="B06124-001-a-2770">their</w>
     <w lemma="duty" pos="n1" xml:id="B06124-001-a-2780">Duty</w>
     <pc xml:id="B06124-001-a-2790">,</pc>
     <w lemma="obedience" pos="n1" xml:id="B06124-001-a-2800">Obedience</w>
     <pc xml:id="B06124-001-a-2810">,</pc>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-2820">and</w>
     <w lemma="allegiance" pos="n1" xml:id="B06124-001-a-2830">Allegiance</w>
     <w lemma="to" pos="acp" xml:id="B06124-001-a-2840">to</w>
     <w lemma="he" pos="pno" xml:id="B06124-001-a-2850">Him</w>
     <pc xml:id="B06124-001-a-2860">,</pc>
     <w lemma="upon" pos="acp" xml:id="B06124-001-a-2870">upon</w>
     <w lemma="a" pos="d" xml:id="B06124-001-a-2880">an</w>
     <w lemma="opinion" pos="n1" xml:id="B06124-001-a-2890">opinion</w>
     <pc xml:id="B06124-001-a-2900">,</pc>
     <w lemma="that" pos="cs" xml:id="B06124-001-a-2910">That</w>
     <w lemma="his" pos="po" xml:id="B06124-001-a-2920">His</w>
     <w lemma="majesty" pos="n1" xml:id="B06124-001-a-2930">Majesty</w>
     <w lemma="can" pos="vmd" xml:id="B06124-001-a-2940">could</w>
     <w lemma="not" pos="xx" xml:id="B06124-001-a-2950">not</w>
     <w lemma="have" pos="vvi" xml:id="B06124-001-a-2960">have</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-2970">the</w>
     <w lemma="benefit" pos="n1" xml:id="B06124-001-a-2980">benefit</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-2990">of</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-3000">the</w>
     <w lemma="law" pos="n2" reg="laws" xml:id="B06124-001-a-3010">Lawes</w>
     <w lemma="here" pos="av" xml:id="B06124-001-a-3020">here</w>
     <w lemma="against" pos="acp" xml:id="B06124-001-a-3030">against</w>
     <w lemma="they" pos="pno" xml:id="B06124-001-a-3040">them</w>
     <pc xml:id="B06124-001-a-3050">,</pc>
     <w lemma="he" pos="pns" xml:id="B06124-001-a-3060">He</w>
     <w lemma="will" pos="vmb" xml:id="B06124-001-a-3070">will</w>
     <w lemma="never" pos="avx" xml:id="B06124-001-a-3080">never</w>
     <w lemma="consent" pos="vvi" xml:id="B06124-001-a-3090">consent</w>
     <w lemma="that" pos="cs" xml:id="B06124-001-a-3100">that</w>
     <w lemma="they" pos="pns" xml:id="B06124-001-a-3110">they</w>
     <w lemma="shall" pos="vmb" xml:id="B06124-001-a-3120">shall</w>
     <w lemma="have" pos="vvi" xml:id="B06124-001-a-3130">have</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-3140">the</w>
     <w lemma="benefit" pos="n1" xml:id="B06124-001-a-3150">benefit</w>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-3160">and</w>
     <w lemma="protection" pos="n1" xml:id="B06124-001-a-3170">Protection</w>
     <w lemma="due" pos="j" xml:id="B06124-001-a-3180">due</w>
     <w lemma="only" pos="av-j" xml:id="B06124-001-a-3190">only</w>
     <w lemma="to" pos="acp" xml:id="B06124-001-a-3200">to</w>
     <w lemma="his" pos="po" xml:id="B06124-001-a-3210">His</w>
     <w lemma="good" pos="j" xml:id="B06124-001-a-3220">good</w>
     <w lemma="subject" pos="n2" xml:id="B06124-001-a-3230">Subjects</w>
     <w lemma="abroad" pos="av" xml:id="B06124-001-a-3240">abroad</w>
     <pc xml:id="B06124-001-a-3250">,</pc>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-3260">and</w>
     <w lemma="so" pos="av" xml:id="B06124-001-a-3270">so</w>
     <w lemma="evade" pos="vvi" xml:id="B06124-001-a-3280">evade</w>
     <w lemma="his" pos="po" xml:id="B06124-001-a-3290">His</w>
     <w lemma="justice" pos="n1" reg="justice" xml:id="B06124-001-a-3300">Iustice</w>
     <w lemma="everywhere" pos="av" reg="everywhere" xml:id="B06124-001-a-3310">every where</w>
     <pc xml:id="B06124-001-a-3330">;</pc>
     <w lemma="but" pos="acp" xml:id="B06124-001-a-3340">but</w>
     <w lemma="will" pos="vmb" xml:id="B06124-001-a-3350">will</w>
     <w lemma="precise" pos="av-j" xml:id="B06124-001-a-3360">precisely</w>
     <w lemma="observe" pos="vvi" xml:id="B06124-001-a-3370">observe</w>
     <w lemma="his" pos="po" xml:id="B06124-001-a-3380">His</w>
     <w lemma="resolution" pos="n1" xml:id="B06124-001-a-3390">resolution</w>
     <w lemma="declare" pos="vvd" xml:id="B06124-001-a-3400">declared</w>
     <w lemma="so" pos="av" xml:id="B06124-001-a-3410">so</w>
     <w lemma="long" pos="av-j" xml:id="B06124-001-a-3420">long</w>
     <w lemma="since" pos="acp" xml:id="B06124-001-a-3430">since</w>
     <pc xml:id="B06124-001-a-3440">,</pc>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-3450">and</w>
     <w lemma="so" pos="av" xml:id="B06124-001-a-3460">so</w>
     <w lemma="gracious" pos="av-j" reg="graciously" xml:id="B06124-001-a-3470">gratiously</w>
     <pc xml:id="B06124-001-a-3480">,</pc>
     <w lemma="in" pos="acp" xml:id="B06124-001-a-3490">in</w>
     <w lemma="answer" pos="n1" xml:id="B06124-001-a-3500">answer</w>
     <w lemma="to" pos="acp" xml:id="B06124-001-a-3510">to</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-3520">the</w>
     <w lemma="petition" pos="n1" xml:id="B06124-001-a-3530">Petition</w>
     <w lemma="present" pos="vvn" xml:id="B06124-001-a-3540">presented</w>
     <w lemma="in" pos="acp" xml:id="B06124-001-a-3550">in</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-3560">the</w>
     <w lemma="beginning" pos="n1" xml:id="B06124-001-a-3570">beginning</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-3580">of</w>
     <w lemma="january" pos="nn1" reg="January" rend="hi" xml:id="B06124-001-a-3590">Ianuary</w>
     <w lemma="last" pos="ord" xml:id="B06124-001-a-3600">last</w>
     <w lemma="from" pos="acp" xml:id="B06124-001-a-3610">from</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-3620">the</w>
     <w lemma="city" pos="n1" xml:id="B06124-001-a-3630">City</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-3640">of</w>
     <hi xml:id="B06124-e290">
      <w lemma="London" pos="nn1" xml:id="B06124-001-a-3650">London</w>
      <pc xml:id="B06124-001-a-3660">,</pc>
     </hi>
     <w lemma="in" pos="acp" xml:id="B06124-001-a-3670">in</w>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-3680">and</w>
     <w lemma="by" pos="acp" xml:id="B06124-001-a-3690">by</w>
     <w lemma="which" pos="crq" xml:id="B06124-001-a-3700">which</w>
     <w lemma="the" pos="d" xml:id="B06124-001-a-3710">the</w>
     <w lemma="petitioner" pos="n2" xml:id="B06124-001-a-3720">Petitioners</w>
     <pc xml:id="B06124-001-a-3730">,</pc>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-3740">and</w>
     <w lemma="all" pos="d" xml:id="B06124-001-a-3750">all</w>
     <w lemma="other" pos="d" xml:id="B06124-001-a-3760">other</w>
     <w lemma="person" pos="n2" xml:id="B06124-001-a-3770">Persons</w>
     <w lemma="concern" pos="vvn" xml:id="B06124-001-a-3780">concerned</w>
     <pc xml:id="B06124-001-a-3790">,</pc>
     <w lemma="have" pos="vvd" xml:id="B06124-001-a-3800">had</w>
     <w lemma="sufficient" pos="j" xml:id="B06124-001-a-3810">sufficient</w>
     <w lemma="warn" pos="vvg" xml:id="B06124-001-a-3820">Warning</w>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-3830">and</w>
     <w lemma="instance" pos="n1" xml:id="B06124-001-a-3840">instance</w>
     <w lemma="of" pos="acp" xml:id="B06124-001-a-3850">of</w>
     <w lemma="his" pos="po" xml:id="B06124-001-a-3860">His</w>
     <w lemma="majesty" pos="ng1" reg="majesty's" xml:id="B06124-001-a-3870">Majesties</w>
     <w lemma="grace" pos="n1" xml:id="B06124-001-a-3880">Grace</w>
     <w lemma="and" pos="cc" xml:id="B06124-001-a-3890">and</w>
     <w lemma="favour" pos="n1" xml:id="B06124-001-a-3900">Favour</w>
     <w lemma="to" pos="acp" xml:id="B06124-001-a-3910">to</w>
     <w lemma="they" pos="pno" xml:id="B06124-001-a-3920">them</w>
     <pc unit="sentence" xml:id="B06124-001-a-3930">.</pc>
    </p>
    <closer xml:id="B06124-e300">
     <signed xml:id="B06124-e310">
      <w lemma="EDW." pos="ab" xml:id="B06124-001-a-3940">EDW.</w>
      <w lemma="NICHOLAS" pos="nn1" xml:id="B06124-001-a-3960">NICHOLAS</w>
      <pc unit="sentence" xml:id="B06124-001-a-3970">.</pc>
     </signed>
    </closer>
   </div>
  </body>
  <back xml:id="B06124-e320">
   <div type="colophon" xml:id="B06124-e330">
    <p xml:id="B06124-e340">
     <w lemma="print" pos="vvn" xml:id="B06124-001-a-3980">Printed</w>
     <w lemma="at" pos="acp" xml:id="B06124-001-a-3990">at</w>
     <w lemma="Oxford" pos="nn1" rend="hi" xml:id="B06124-001-a-4000">Oxford</w>
     <w lemma="by" pos="acp" xml:id="B06124-001-a-4010">by</w>
     <w lemma="LEONARD" pos="nn1" xml:id="B06124-001-a-4020">LEONARD</w>
     <w lemma="lichfinld" pos="vmd" xml:id="B06124-001-a-4030">LICHFINLD</w>
     <pc unit="sentence" xml:id="B06124-001-a-4040">.</pc>
     <w lemma="1643." pos="crd" xml:id="B06124-001-a-4050">1643.</w>
     <pc unit="sentence" xml:id="B06124-001-a-4060"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
