<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B06620">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>His Majesties most gracious speech to both houses of Parliament, on February first, 1699.</title>
    <author>England and Wales. Sovereign (1694-1702 : William II)</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B06620 of text R186681 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing W2418). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 2 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B06620</idno>
    <idno type="STC">Wing W2418</idno>
    <idno type="STC">ESTC R186681</idno>
    <idno type="EEBO-CITATION">52529374</idno>
    <idno type="OCLC">ocm 52529374</idno>
    <idno type="VID">179261</idno>
    <idno type="PROQUESTGOID">2240862264</idno>
    <availability>
     <p>This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. Searching, reading, printing, or downloading EEBO-TCP texts is reserved for the authorized users of these project partner institutions. Permission must be granted for subsequent distribution, in print or electronically, of this EEBO-TCP Phase II text, in whole or in part.</p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 2, no. B06620)</note>
    <note>Transcribed from: (Early English Books Online ; image set 179261)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2781:28)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>His Majesties most gracious speech to both houses of Parliament, on February first, 1699.</title>
      <author>England and Wales. Sovereign (1694-1702 : William II)</author>
      <author>William III, King of England, 1650-1702.</author>
      <author>England and Wales. Parliament.</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Re-printed by the heirs and successors of Andrew Anderson, Printer to the King's most excellent Majesty,</publisher>
      <pubPlace>Edinburgh :</pubPlace>
      <date>Anno Dom. 1699.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Royal arms at head of text.</note>
      <note>Reproduction of the original in the National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>England and Wales. -- Army -- Early works to 1800.</term>
     <term>Great Britain -- Politics and government -- 1689-1702 -- Early works to 1800.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>His Majesties most gracious speech to both Houses of Parliament, on February first, 1699.</ep:title>
    <ep:author>England and Wales. Sovereign </ep:author>
    <ep:publicationYear>1699</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>316</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-02</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-03</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-04</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-10</date>
    <label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change>
    <date>2009-07</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2009-07</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-09</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B06620-e10010">
  <body xml:id="B06620-e10020">
   <div type="speech" xml:id="B06620-e10030">
    <pb facs="tcp:179261:1" rend="simple:additions" xml:id="B06620-001-a"/>
    <head xml:id="B06620-e10040">
     <figure xml:id="B06620-e10050">
      <head xml:id="B06620-e10060">
       <w lemma="w" pos="ab" xml:id="B06620-001-a-0010">W</w>
       <w lemma="r" pos="sy" xml:id="B06620-001-a-0020">R</w>
      </head>
      <q xml:id="B06620-e10070">
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0030">DIEV</w>
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0040">ET</w>
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0050">MON</w>
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0060">DROIT</w>
      </q>
      <q xml:id="B06620-e10080">
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0070">HONI</w>
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0080">SOIT</w>
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0090">QUI</w>
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0100">MAL</w>
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0110">Y</w>
       <w lemma="n/a" pos="ffr" xml:id="B06620-001-a-0120">PENSE</w>
      </q>
      <figDesc xml:id="B06620-e10090">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="B06620-e10100">
     <w lemma="his" pos="po" xml:id="B06620-001-a-0130">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="B06620-001-a-0140">Majesties</w>
     <w lemma="most" pos="avs-d" xml:id="B06620-001-a-0150">MOST</w>
     <w lemma="gracious" pos="j" xml:id="B06620-001-a-0160">GRACIOUS</w>
     <w lemma="speech" pos="n1" xml:id="B06620-001-a-0170">SPEECH</w>
     <w lemma="to" pos="acp" xml:id="B06620-001-a-0180">To</w>
     <w lemma="both" pos="d" xml:id="B06620-001-a-0190">both</w>
     <w lemma="house" pos="n2" xml:id="B06620-001-a-0200">HOUSES</w>
     <w lemma="of" pos="acp" xml:id="B06620-001-a-0210">of</w>
     <w lemma="parliament" pos="n1" xml:id="B06620-001-a-0220">PARLIAMENT</w>
     <pc xml:id="B06620-001-a-0230">,</pc>
     <w lemma="on" pos="acp" xml:id="B06620-001-a-0240">On</w>
     <date xml:id="B06620-e10110">
      <w lemma="February" pos="nn1" rend="hi" xml:id="B06620-001-a-0250">February</w>
      <w lemma="first" pos="ord" xml:id="B06620-001-a-0260">First</w>
      <pc xml:id="B06620-001-a-0270">,</pc>
      <w lemma="1699." pos="crd" xml:id="B06620-001-a-0280">1699.</w>
      <pc unit="sentence" xml:id="B06620-001-a-0290"/>
     </date>
    </head>
    <opener xml:id="B06620-e10130">
     <salute xml:id="B06620-e10140">
      <w lemma="my" pos="po" xml:id="B06620-001-a-0300">My</w>
      <w lemma="lord" pos="n2" xml:id="B06620-001-a-0310">Lords</w>
      <w lemma="and" pos="cc" xml:id="B06620-001-a-0320">and</w>
      <w lemma="gentleman" pos="n2" xml:id="B06620-001-a-0330">Gentlemen</w>
      <pc xml:id="B06620-001-a-0340">,</pc>
     </salute>
    </opener>
    <p xml:id="B06620-e10150">
     <w lemma="I" pos="pns" xml:id="B06620-001-a-0350">I</w>
     <w lemma="come" pos="vvd" xml:id="B06620-001-a-0360">Came</w>
     <w lemma="to" pos="prt" xml:id="B06620-001-a-0370">to</w>
     <w lemma="pass" pos="vvi" xml:id="B06620-001-a-0380">Pass</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-0390">the</w>
     <w lemma="bill" pos="n1" xml:id="B06620-001-a-0400">Bill</w>
     <w lemma="for" pos="acp" xml:id="B06620-001-a-0410">for</w>
     <w lemma="disband" pos="vvg" xml:id="B06620-001-a-0420">Disbanding</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-0430">the</w>
     <w lemma="army" pos="n1" xml:id="B06620-001-a-0440">Army</w>
     <pc xml:id="B06620-001-a-0450">,</pc>
     <w lemma="as" pos="acp" xml:id="B06620-001-a-0460">as</w>
     <w lemma="soon" pos="av" xml:id="B06620-001-a-0470">soon</w>
     <w lemma="as" pos="acp" xml:id="B06620-001-a-0480">as</w>
     <w lemma="I" pos="pns" xml:id="B06620-001-a-0490">I</w>
     <w lemma="understand" pos="vvd" xml:id="B06620-001-a-0500">understood</w>
     <w lemma="it" pos="pn" xml:id="B06620-001-a-0510">it</w>
     <w lemma="be" pos="vvd" xml:id="B06620-001-a-0520">was</w>
     <w lemma="ready" pos="j" xml:id="B06620-001-a-0530">ready</w>
     <w lemma="for" pos="acp" xml:id="B06620-001-a-0540">for</w>
     <w lemma="me." pos="ab" xml:id="B06620-001-a-0550">Me.</w>
     <pc unit="sentence" xml:id="B06620-001-a-0560"/>
    </p>
    <p xml:id="B06620-e10160">
     <w lemma="though" pos="cs" xml:id="B06620-001-a-0570">Though</w>
     <w lemma="in" pos="acp" xml:id="B06620-001-a-0580">in</w>
     <w lemma="our" pos="po" xml:id="B06620-001-a-0590">our</w>
     <w lemma="present" pos="j" xml:id="B06620-001-a-0600">Present</w>
     <w lemma="circumstance" pos="n2" xml:id="B06620-001-a-0610">Circumstances</w>
     <w lemma="there" pos="av" xml:id="B06620-001-a-0620">there</w>
     <w lemma="appear" pos="vvz" xml:id="B06620-001-a-0630">appears</w>
     <w lemma="great" pos="j" xml:id="B06620-001-a-0640">great</w>
     <w lemma="hazard" pos="n1" xml:id="B06620-001-a-0650">Hazard</w>
     <w lemma="in" pos="acp" xml:id="B06620-001-a-0660">in</w>
     <w lemma="break" pos="vvg" xml:id="B06620-001-a-0670">breaking</w>
     <w lemma="such" pos="d" xml:id="B06620-001-a-0680">such</w>
     <w lemma="a" pos="d" xml:id="B06620-001-a-0690">a</w>
     <w lemma="number" pos="n1" xml:id="B06620-001-a-0700">Number</w>
     <w lemma="of" pos="acp" xml:id="B06620-001-a-0710">of</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-0720">the</w>
     <w lemma="troop" pos="n2" xml:id="B06620-001-a-0730">Troops</w>
     <pc xml:id="B06620-001-a-0740">;</pc>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-0750">and</w>
     <w lemma="though" pos="cs" xml:id="B06620-001-a-0760">though</w>
     <w lemma="I" pos="pns" xml:id="B06620-001-a-0770">I</w>
     <w lemma="may" pos="vmd" xml:id="B06620-001-a-0780">might</w>
     <w lemma="think" pos="vvi" xml:id="B06620-001-a-0790">think</w>
     <w lemma="myself" pos="pr" xml:id="B06620-001-a-0800">MySelf</w>
     <w lemma="unkind" pos="av-j" xml:id="B06620-001-a-0810">unkindly</w>
     <w lemma="use" pos="vvn" xml:id="B06620-001-a-0820">used</w>
     <pc xml:id="B06620-001-a-0830">,</pc>
     <w lemma="that" pos="cs" xml:id="B06620-001-a-0840">That</w>
     <w lemma="those" pos="d" xml:id="B06620-001-a-0850">those</w>
     <w lemma="guard" pos="n2" xml:id="B06620-001-a-0860">Guards</w>
     <pc xml:id="B06620-001-a-0870">,</pc>
     <w lemma="who" pos="crq" xml:id="B06620-001-a-0880">who</w>
     <w lemma="come" pos="vvd" xml:id="B06620-001-a-0890">came</w>
     <w lemma="over" pos="acp" xml:id="B06620-001-a-0900">over</w>
     <w lemma="with" pos="acp" xml:id="B06620-001-a-0910">with</w>
     <w lemma="i" pos="pno" xml:id="B06620-001-a-0920">Me</w>
     <w lemma="to" pos="acp" xml:id="B06620-001-a-0930">to</w>
     <w lemma="your" pos="po" xml:id="B06620-001-a-0940">your</w>
     <w lemma="assistance" pos="n1" xml:id="B06620-001-a-0950">Assistance</w>
     <pc xml:id="B06620-001-a-0960">,</pc>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-0970">and</w>
     <w lemma="have" pos="vvb" xml:id="B06620-001-a-0980">have</w>
     <w lemma="constant" pos="av-j" xml:id="B06620-001-a-0990">constantly</w>
     <w lemma="attend" pos="vvn" xml:id="B06620-001-a-1000">Attended</w>
     <w lemma="i" pos="pno" xml:id="B06620-001-a-1010">Me</w>
     <w lemma="in" pos="acp" xml:id="B06620-001-a-1020">in</w>
     <w lemma="all" pos="d" xml:id="B06620-001-a-1030">all</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-1040">the</w>
     <w lemma="action" pos="n2" xml:id="B06620-001-a-1050">Actions</w>
     <w lemma="wherein" pos="crq" xml:id="B06620-001-a-1060">wherein</w>
     <w lemma="I" pos="pns" xml:id="B06620-001-a-1070">I</w>
     <w lemma="have" pos="vvb" xml:id="B06620-001-a-1080">have</w>
     <w lemma="be" pos="vvn" xml:id="B06620-001-a-1090">been</w>
     <w lemma="engage" pos="vvn" xml:id="B06620-001-a-1100">Engaged</w>
     <pc xml:id="B06620-001-a-1110">,</pc>
     <w lemma="shall" pos="vmd" xml:id="B06620-001-a-1120">should</w>
     <w lemma="be" pos="vvi" xml:id="B06620-001-a-1130">be</w>
     <w lemma="remove" pos="vvn" xml:id="B06620-001-a-1140">Removed</w>
     <w lemma="from" pos="acp" xml:id="B06620-001-a-1150">from</w>
     <w lemma="i" pos="pno" xml:id="B06620-001-a-1160">Me</w>
     <pc xml:id="B06620-001-a-1170">;</pc>
     <w lemma="yet" pos="av" xml:id="B06620-001-a-1180">yet</w>
     <w lemma="it" pos="pn" xml:id="B06620-001-a-1190">it</w>
     <w lemma="be" pos="vvz" xml:id="B06620-001-a-1200">is</w>
     <w lemma="my" pos="po" xml:id="B06620-001-a-1210">My</w>
     <w lemma="fix" pos="j-vn" reg="fixed" xml:id="B06620-001-a-1220">fix'd</w>
     <w lemma="opinion" pos="n1" xml:id="B06620-001-a-1230">Opinion</w>
     <pc xml:id="B06620-001-a-1240">,</pc>
     <w lemma="that" pos="cs" xml:id="B06620-001-a-1250">That</w>
     <w lemma="nothing" pos="pix" xml:id="B06620-001-a-1260">nothing</w>
     <w lemma="can" pos="vmb" xml:id="B06620-001-a-1270">can</w>
     <w lemma="be" pos="vvi" xml:id="B06620-001-a-1280">be</w>
     <w lemma="so" pos="av" xml:id="B06620-001-a-1290">so</w>
     <w lemma="fatal" pos="j" xml:id="B06620-001-a-1300">Fatal</w>
     <w lemma="to" pos="acp" xml:id="B06620-001-a-1310">to</w>
     <w lemma="we" pos="pno" xml:id="B06620-001-a-1320">Us</w>
     <pc xml:id="B06620-001-a-1330">,</pc>
     <w lemma="as" pos="acp" xml:id="B06620-001-a-1340">as</w>
     <w lemma="that" pos="cs" xml:id="B06620-001-a-1350">that</w>
     <w lemma="any" pos="d" xml:id="B06620-001-a-1360">any</w>
     <w lemma="distrust" pos="n1" xml:id="B06620-001-a-1370">Distrust</w>
     <w lemma="or" pos="cc" xml:id="B06620-001-a-1380">or</w>
     <w lemma="jealousy" pos="n1" xml:id="B06620-001-a-1390">Jealousy</w>
     <w lemma="shall" pos="vmd" xml:id="B06620-001-a-1400">should</w>
     <w lemma="arise" pos="vvi" xml:id="B06620-001-a-1410">arise</w>
     <w lemma="between" pos="acp" reg="between" xml:id="B06620-001-a-1420">betwen</w>
     <w lemma="i" pos="pno" xml:id="B06620-001-a-1430">Me</w>
     <pc xml:id="B06620-001-a-1440">,</pc>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-1450">and</w>
     <w lemma="my" pos="po" xml:id="B06620-001-a-1460">My</w>
     <w lemma="people" pos="n1" xml:id="B06620-001-a-1470">People</w>
     <pc xml:id="B06620-001-a-1480">,</pc>
     <w lemma="which" pos="crq" xml:id="B06620-001-a-1490">which</w>
     <w lemma="I" pos="pns" xml:id="B06620-001-a-1500">I</w>
     <w lemma="must" pos="vmb" xml:id="B06620-001-a-1510">must</w>
     <w lemma="own" pos="d" xml:id="B06620-001-a-1520">own</w>
     <w lemma="will" pos="vmd" xml:id="B06620-001-a-1530">would</w>
     <w lemma="have" pos="vvi" xml:id="B06620-001-a-1540">have</w>
     <w lemma="be" pos="vvn" xml:id="B06620-001-a-1550">been</w>
     <w lemma="very" pos="av" xml:id="B06620-001-a-1560">very</w>
     <w lemma="unexpected" pos="j" xml:id="B06620-001-a-1570">Unexpected</w>
     <w lemma="after" pos="acp" xml:id="B06620-001-a-1580">after</w>
     <w lemma="what" pos="crq" xml:id="B06620-001-a-1590">what</w>
     <w lemma="I" pos="pns" xml:id="B06620-001-a-1600">I</w>
     <w lemma="have" pos="vvb" xml:id="B06620-001-a-1610">have</w>
     <w lemma="undertake" pos="vvn" xml:id="B06620-001-a-1620">Undertaken</w>
     <pc xml:id="B06620-001-a-1630">,</pc>
     <w lemma="venture" pos="vvn" xml:id="B06620-001-a-1640">Ventured</w>
     <pc xml:id="B06620-001-a-1650">,</pc>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-1660">and</w>
     <w lemma="act" pos="vvn" xml:id="B06620-001-a-1670">Acted</w>
     <pc xml:id="B06620-001-a-1680">,</pc>
     <w lemma="for" pos="acp" xml:id="B06620-001-a-1690">for</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-1700">the</w>
     <w lemma="restore" pos="j-vg" xml:id="B06620-001-a-1710">Restoring</w>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-1720">and</w>
     <w lemma="secure" pos="vvg" xml:id="B06620-001-a-1730">Securing</w>
     <w lemma="of" pos="acp" xml:id="B06620-001-a-1740">of</w>
     <w lemma="their" pos="po" xml:id="B06620-001-a-1750">their</w>
     <w lemma="liberty" pos="n2" xml:id="B06620-001-a-1760">Liberties</w>
     <pc unit="sentence" xml:id="B06620-001-a-1770">.</pc>
    </p>
    <p xml:id="B06620-e10170">
     <w lemma="I" pos="pns" xml:id="B06620-001-a-1780">I</w>
     <w lemma="have" pos="vvb" xml:id="B06620-001-a-1790">have</w>
     <w lemma="thus" pos="av" xml:id="B06620-001-a-1800">thus</w>
     <w lemma="plain" pos="av-j" xml:id="B06620-001-a-1810">Plainly</w>
     <w lemma="tell" pos="vvd" xml:id="B06620-001-a-1820">told</w>
     <w lemma="you" pos="pn" xml:id="B06620-001-a-1830">you</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-1840">the</w>
     <w lemma="only" pos="j" xml:id="B06620-001-a-1850">only</w>
     <w lemma="reason" pos="n1" xml:id="B06620-001-a-1860">Reason</w>
     <pc xml:id="B06620-001-a-1870">,</pc>
     <w lemma="which" pos="crq" xml:id="B06620-001-a-1880">which</w>
     <w lemma="have" pos="vvz" xml:id="B06620-001-a-1890">has</w>
     <w lemma="induce" pos="vvn" xml:id="B06620-001-a-1900">induced</w>
     <w lemma="i" pos="pno" xml:id="B06620-001-a-1910">Me</w>
     <w lemma="to" pos="prt" xml:id="B06620-001-a-1920">to</w>
     <w lemma="pass" pos="vvi" xml:id="B06620-001-a-1930">Pass</w>
     <w lemma="this" pos="d" xml:id="B06620-001-a-1940">this</w>
     <w lemma="bill" pos="n1" xml:id="B06620-001-a-1950">Bill</w>
     <pc xml:id="B06620-001-a-1960">;</pc>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-1970">And</w>
     <w lemma="now" pos="av" xml:id="B06620-001-a-1980">now</w>
     <w lemma="I" pos="pns" xml:id="B06620-001-a-1990">I</w>
     <w lemma="think" pos="vvb" xml:id="B06620-001-a-2000">think</w>
     <w lemma="myself" pos="pr" xml:id="B06620-001-a-2010">Myself</w>
     <w lemma="oblige" pos="vvn" xml:id="B06620-001-a-2020">Obliged</w>
     <pc xml:id="B06620-001-a-2030">,</pc>
     <w lemma="in" pos="acp" xml:id="B06620-001-a-2040">in</w>
     <w lemma="discharge" pos="n1" xml:id="B06620-001-a-2050">Discharge</w>
     <w lemma="of" pos="acp" xml:id="B06620-001-a-2060">of</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-2070">the</w>
     <w lemma="trust" pos="vvb" xml:id="B06620-001-a-2080">Trust</w>
     <w lemma="repose" pos="vvn" xml:id="B06620-001-a-2090">reposed</w>
     <w lemma="in" pos="acp" xml:id="B06620-001-a-2100">in</w>
     <w lemma="i" pos="pno" xml:id="B06620-001-a-2110">Me</w>
     <pc xml:id="B06620-001-a-2120">,</pc>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-2130">and</w>
     <w lemma="for" pos="acp" xml:id="B06620-001-a-2140">for</w>
     <w lemma="my" pos="po" xml:id="B06620-001-a-2150">My</w>
     <w lemma="own" pos="d" xml:id="B06620-001-a-2160">own</w>
     <w lemma="justification" pos="n1" xml:id="B06620-001-a-2170">Justification</w>
     <pc xml:id="B06620-001-a-2180">,</pc>
     <w lemma="that" pos="cs" xml:id="B06620-001-a-2190">that</w>
     <w lemma="no" pos="dx" xml:id="B06620-001-a-2200">no</w>
     <w lemma="ill" pos="j" xml:id="B06620-001-a-2210">Ill</w>
     <w lemma="consequence" pos="n2" xml:id="B06620-001-a-2220">Consequences</w>
     <w lemma="may" pos="vmb" xml:id="B06620-001-a-2230">may</w>
     <w lemma="lie" pos="vvi" xml:id="B06620-001-a-2240">lie</w>
     <w lemma="at" pos="acp" xml:id="B06620-001-a-2250">at</w>
     <w lemma="my" pos="po" xml:id="B06620-001-a-2260">My</w>
     <w lemma="door" pos="n1" xml:id="B06620-001-a-2270">Door</w>
     <pc xml:id="B06620-001-a-2280">,</pc>
     <w lemma="to" pos="prt" xml:id="B06620-001-a-2290">to</w>
     <w lemma="tell" pos="vvi" xml:id="B06620-001-a-2300">tell</w>
     <w lemma="you" pos="pn" xml:id="B06620-001-a-2310">you</w>
     <w lemma="as" pos="acp" xml:id="B06620-001-a-2320">as</w>
     <w lemma="plain" pos="av-j" xml:id="B06620-001-a-2330">plainly</w>
     <w lemma="my" pos="po" xml:id="B06620-001-a-2340">My</w>
     <w lemma="judgement" pos="n1" reg="Judgement" xml:id="B06620-001-a-2350">Judgment</w>
     <pc xml:id="B06620-001-a-2360">,</pc>
     <w lemma="that" pos="cs" xml:id="B06620-001-a-2370">That</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-2380">the</w>
     <w lemma="nation" pos="n1" xml:id="B06620-001-a-2390">Nation</w>
     <w lemma="be" pos="vvz" xml:id="B06620-001-a-2400">is</w>
     <w lemma="leave" pos="vvn" xml:id="B06620-001-a-2410">left</w>
     <w lemma="too" pos="av" xml:id="B06620-001-a-2420">too</w>
     <w lemma="much" pos="av-d" xml:id="B06620-001-a-2430">much</w>
     <w lemma="expose" pos="vvn" xml:id="B06620-001-a-2440">Exposed</w>
     <pc unit="sentence" xml:id="B06620-001-a-2450">.</pc>
    </p>
    <p xml:id="B06620-e10180">
     <w lemma="it" pos="pn" xml:id="B06620-001-a-2460">It</w>
     <w lemma="be" pos="vvz" xml:id="B06620-001-a-2470">is</w>
     <w lemma="therefore" pos="av" xml:id="B06620-001-a-2480">therefore</w>
     <w lemma="incumbent" pos="j" xml:id="B06620-001-a-2490">Incumbent</w>
     <w lemma="upon" pos="acp" xml:id="B06620-001-a-2500">upon</w>
     <w lemma="you" pos="pn" xml:id="B06620-001-a-2510">you</w>
     <pc xml:id="B06620-001-a-2520">,</pc>
     <w lemma="to" pos="prt" xml:id="B06620-001-a-2530">to</w>
     <w lemma="take" pos="vvi" xml:id="B06620-001-a-2540">take</w>
     <w lemma="this" pos="d" xml:id="B06620-001-a-2550">this</w>
     <w lemma="matter" pos="n1" xml:id="B06620-001-a-2560">Matter</w>
     <w lemma="into" pos="acp" xml:id="B06620-001-a-2570">into</w>
     <w lemma="your" pos="po" xml:id="B06620-001-a-2580">your</w>
     <w lemma="serious" pos="j" xml:id="B06620-001-a-2590">Serious</w>
     <w lemma="consideration" pos="n1" xml:id="B06620-001-a-2600">Consideration</w>
     <pc xml:id="B06620-001-a-2610">,</pc>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-2620">and</w>
     <w lemma="effectual" pos="av-j" xml:id="B06620-001-a-2630">effectually</w>
     <w lemma="to" pos="prt" xml:id="B06620-001-a-2640">to</w>
     <w lemma="provide" pos="vvb" xml:id="B06620-001-a-2650">Provide</w>
     <w lemma="such" pos="d" xml:id="B06620-001-a-2660">such</w>
     <w lemma="a" pos="d" xml:id="B06620-001-a-2670">a</w>
     <w lemma="strength" pos="n1" xml:id="B06620-001-a-2680">Strength</w>
     <pc xml:id="B06620-001-a-2690">,</pc>
     <w lemma="as" pos="acp" xml:id="B06620-001-a-2700">as</w>
     <w lemma="be" pos="vvz" xml:id="B06620-001-a-2710">is</w>
     <w lemma="necessary" pos="j" xml:id="B06620-001-a-2720">necessary</w>
     <w lemma="for" pos="acp" xml:id="B06620-001-a-2730">for</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-2740">the</w>
     <w lemma="safety" pos="n1" xml:id="B06620-001-a-2750">Safety</w>
     <w lemma="of" pos="acp" xml:id="B06620-001-a-2760">of</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-2770">the</w>
     <w lemma="kingdom" pos="n1" xml:id="B06620-001-a-2780">Kingdom</w>
     <pc xml:id="B06620-001-a-2790">,</pc>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-2800">and</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-2810">the</w>
     <w lemma="preservation" pos="n1" xml:id="B06620-001-a-2820">Preservation</w>
     <w lemma="of" pos="acp" xml:id="B06620-001-a-2830">of</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-2840">the</w>
     <w lemma="peace" pos="n1" xml:id="B06620-001-a-2850">Peace</w>
     <w lemma="which" pos="crq" xml:id="B06620-001-a-2860">which</w>
     <w lemma="GOD" pos="nn1" xml:id="B06620-001-a-2870">GOD</w>
     <w lemma="have" pos="vvz" xml:id="B06620-001-a-2880">hath</w>
     <w lemma="give" pos="vvn" xml:id="B06620-001-a-2890">given</w>
     <w lemma="Us." pos="nn1" xml:id="B06620-001-a-2900">Us.</w>
     <pc unit="sentence" xml:id="B06620-001-a-2910"/>
    </p>
   </div>
  </body>
  <back xml:id="B06620-e10190">
   <div type="colophon" xml:id="B06620-e10200">
    <p xml:id="B06620-e10210">
     <w lemma="Edinburgh" pos="nn1" xml:id="B06620-001-a-2920">EDINBURGH</w>
     <pc xml:id="B06620-001-a-2930">,</pc>
     <w lemma="reprint" pos="vvn" reg="reprinted" xml:id="B06620-001-a-2940">Re-printed</w>
     <w lemma="by" pos="acp" xml:id="B06620-001-a-2950">by</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-2960">the</w>
     <w lemma="heir" pos="n2" xml:id="B06620-001-a-2970">Heirs</w>
     <w lemma="and" pos="cc" xml:id="B06620-001-a-2980">and</w>
     <w lemma="successor" pos="n2" xml:id="B06620-001-a-2990">Successors</w>
     <w lemma="of" pos="acp" xml:id="B06620-001-a-3000">of</w>
     <hi xml:id="B06620-e10220">
      <w lemma="Andrew" pos="nn1" xml:id="B06620-001-a-3010">Andrew</w>
      <w lemma="Anderson" pos="nn1" xml:id="B06620-001-a-3020">Anderson</w>
     </hi>
     <pc rend="follows-hi" xml:id="B06620-001-a-3030">,</pc>
     <w lemma="printer" pos="n1" xml:id="B06620-001-a-3040">Printer</w>
     <w lemma="to" pos="acp" xml:id="B06620-001-a-3050">to</w>
     <w lemma="the" pos="d" xml:id="B06620-001-a-3060">the</w>
     <w join="right" lemma="king" pos="n1" xml:id="B06620-001-a-3070">King</w>
     <w join="left" lemma="be" pos="vvz" xml:id="B06620-001-a-3071">'s</w>
     <w lemma="most" pos="avs-d" xml:id="B06620-001-a-3080">most</w>
     <w lemma="excellent" pos="j" xml:id="B06620-001-a-3090">Excellent</w>
     <w lemma="majesty" pos="n1" xml:id="B06620-001-a-3100">Majesty</w>
     <pc xml:id="B06620-001-a-3110">,</pc>
     <hi xml:id="B06620-e10230">
      <w lemma="n/a" pos="fla" xml:id="B06620-001-a-3120">Anno</w>
      <w lemma="dom." pos="ab" xml:id="B06620-001-a-3130">Dom.</w>
     </hi>
     <w lemma="1699." pos="crd" xml:id="B06620-001-a-3140">1699.</w>
     <pc unit="sentence" xml:id="B06620-001-a-3150"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
