<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://schemata.earlyprint.org/schemata/tei_earlyprint.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="B06120">
 <teiHeader>
  <fileDesc>
   <titleStmt>
    <title>To the Kings most excellent Majesty, the humble address of the Lord Mayor, Aldermen, and Commons of the city of London, in Common Council assembled</title>
    <author>City of London (England). Court of Common Council.</author>
   </titleStmt>
   <editionStmt>
    <edition>This text is an enriched version of the TCP digital transcription B06120 of text R185302 in the <ref target="http://estc.bl.uk">English Short Title Catalog</ref> (Wing T1507). Textual changes  and metadata enrichments aim at making the                             text more  computationally tractable, easier to read, and suitable for network-based collaborative                              curation by amateur and professional end users from many walks of life.  The text has been tokenized and linguistically annotated with <ref target="http://morphadorner.northwestern.edu/morphadorner/">MorphAdorner</ref>. The annotation includes standard spellings that support the display of a text in                             a standardized format that preserves archaic forms ('loveth', 'seekest').                              Textual changes aim at restoring the text the author or stationer meant to publish.</edition>
    <respStmt>
     <resp>This text has not been fully proofread</resp>
     <name/>
    </respStmt>
   </editionStmt>
   <extent>Approx. 3 KB of XML-encoded text transcribed from 1 1-bit group-IV TIFF page image.</extent>
   <publicationStmt>
    <publisher>EarlyPrint Project</publisher>
    <pubPlace>Evanston,IL, Notre Dame, IN, St. Louis, MO</pubPlace>
    <date>2018</date>
    <idno type="DLPS">B06120</idno>
    <idno type="STC">Wing T1507</idno>
    <idno type="STC">ESTC R185302</idno>
    <idno type="EEBO-CITATION">53299318</idno>
    <idno type="OCLC">ocm 53299318</idno>
    <idno type="VID">180045</idno>
    <idno type="PROQUESTGOID">2240879393</idno>
    <availability>
     <p>
            This keyboarded and encoded edition of the work described above is co-owned by the institutions providing financial support to the Early English Books Online Text Creation Partnership. This Phase I text is available for reuse, according to the terms of
            <ref target="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons 0 1.0 Universal</ref>
            . The text can be copied, modified, distributed and performed, even for commercial purposes, all without asking permission.
          </p>
    </availability>
   </publicationStmt>
   <seriesStmt>
    <title>Early English books online.</title>
   </seriesStmt>
   <notesStmt>
    <note>(EEBO-TCP ; phase 1, no. B06120)</note>
    <note>Transcribed from: (Early English Books Online ; image set 180045)</note>
    <note>Images scanned from microfilm: (Early English Books, 1641-1700 ; 2811:12)</note>
   </notesStmt>
   <sourceDesc>
    <biblFull>
     <titleStmt>
      <title>To the Kings most excellent Majesty, the humble address of the Lord Mayor, Aldermen, and Commons of the city of London, in Common Council assembled</title>
      <author>City of London (England). Court of Common Council.</author>
      <author>City of London (England). Lord Mayor.</author>
      <author>City of London (England). Court of Aldermen.</author>
      <author>Scotland Sovereign (1660-1685 : Charles II).</author>
     </titleStmt>
     <extent>1 sheet ([1] p.)</extent>
     <publicationStmt>
      <publisher>Re-printed [s.n.],</publisher>
      <pubPlace>Edinburgh, :</pubPlace>
      <date>in the year of God, 1683.</date>
     </publicationStmt>
     <notesStmt>
      <note>Caption title.</note>
      <note>Title vignette; initial letter.</note>
      <note>Signed at end: L. Jenkins.</note>
      <note>Reproduction of original in: National Library of Scotland.</note>
     </notesStmt>
    </biblFull>
   </sourceDesc>
  </fileDesc>
  <profileDesc>
   <langUsage>
    <language ident="eng">eng</language>
   </langUsage>
   <textClass>
    <keywords scheme="http://authorities.loc.gov/">
     <term>Conspiracies -- Great Britain -- Early works to 1800.</term>
     <term>Great Britain -- History -- Charles II, 1660-1685 -- Sources.</term>
     <term>Broadsides -- Scotland -- 17th century.</term>
    </keywords>
   </textClass>
  </profileDesc>
  <xenoData>
   <ep:epHeader xmlns:ep="http://earlyprint.org/ns/1.0">
    <ep:corpus>eebo</ep:corpus>
    <ep:title>To the Kings most excellent Majesty, the humble address of the Lord Mayor, Aldermen, and Commons of the city of London, in Common Council assembled</ep:title>
    <ep:author>City of London . Court of Common Council</ep:author>
    <ep:publicationYear>1683</ep:publicationYear>
    <ep:creationYear/>
    <ep:curator>
     <ep:name/>
    </ep:curator>
    <ep:pageCount>1</ep:pageCount>
    <ep:wordCount>383</ep:wordCount>
    <ep:defectiveTokenCount>0</ep:defectiveTokenCount>
    <ep:untranscribedForeignCount>0</ep:untranscribedForeignCount>
    <ep:untranscribedMathCount>0</ep:untranscribedMathCount>
    <ep:untranscribedMusicCount>0</ep:untranscribedMusicCount>
    <ep:missingChunkCount>0</ep:missingChunkCount>
    <ep:missingPagesCount>0</ep:missingPagesCount>
    <ep:defectRate>0</ep:defectRate>
    <ep:finalGrade>A</ep:finalGrade>
    <ep:defectRangePerGrade>This text  has no known defects that were recorded as gap elements at the time of transcription. </ep:defectRangePerGrade>
   </ep:epHeader>
  </xenoData>
  <revisionDesc>
   <change>
    <date>2008-01</date>
    <label>TCP</label>
        Assigned for keying and markup
      </change>
   <change>
    <date>2008-01</date>
    <label>SPi Global</label>
        Keyed and coded from ProQuest page images
      </change>
   <change>
    <date>2008-02</date>
    <label>Emma (Leeson) Huber</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-08</date>
    <label>SPi Global</label>
        Rekeyed and resubmitted
      </change>
   <change>
    <date>2008-10</date>
    <label>Mona Logarbo</label>
        Sampled and proofread
      </change>
   <change>
    <date>2008-10</date>
    <label>Mona Logarbo</label>
        Text and markup reviewed and edited
      </change>
   <change>
    <date>2009-02</date>
    <label>pfs</label>
        Batch review (QC) and XML conversion
      </change>
  </revisionDesc>
 </teiHeader>
 <text xml:id="B06120-t">
  <body xml:id="B06120-e0">
   <div type="prayer" xml:id="B06120-e10">
    <pb facs="tcp:180045:1" xml:id="B06120-001-a"/>
    <head xml:id="B06120-e20">
     <figure xml:id="B06120-e30">
      <figDesc xml:id="B06120-e40">royal blazon or coat of arms</figDesc>
     </figure>
    </head>
    <head xml:id="B06120-e50">
     <w lemma="to" pos="acp" xml:id="B06120-001-a-0010">TO</w>
     <w lemma="the" pos="d" xml:id="B06120-001-a-0020">THE</w>
     <w lemma="king" pos="ng1" reg="KING'S" rend="hi" xml:id="B06120-001-a-0030">KINGS</w>
     <w lemma="most" pos="avs-d" xml:id="B06120-001-a-0040">Most</w>
     <w lemma="excellent" pos="j" xml:id="B06120-001-a-0050">Excellent</w>
     <hi xml:id="B06120-e70">
      <w lemma="majesty" pos="n1" xml:id="B06120-001-a-0060">MAJESTY</w>
      <pc xml:id="B06120-001-a-0070">,</pc>
     </hi>
     <w lemma="the" pos="d" xml:id="B06120-001-a-0080">THE</w>
     <w lemma="humble" pos="j" xml:id="B06120-001-a-0090">HUMBLE</w>
     <w lemma="address" pos="n1" xml:id="B06120-001-a-0100">ADDRESS</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-0110">OF</w>
     <w lemma="the" pos="d" xml:id="B06120-001-a-0120">THE</w>
     <hi xml:id="B06120-e80">
      <w lemma="lord" pos="n1" xml:id="B06120-001-a-0130">LORD</w>
      <w lemma="mayor" pos="n1" xml:id="B06120-001-a-0140">Mayor</w>
      <pc xml:id="B06120-001-a-0150">,</pc>
      <w lemma="alderman" pos="n2" reg="aldermans" xml:id="B06120-001-a-0160">Aldermen</w>
      <pc xml:id="B06120-001-a-0170">,</pc>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-0180">and</w>
      <w lemma="commons" pos="n2" xml:id="B06120-001-a-0190">Commons</w>
      <w lemma="of" pos="acp" xml:id="B06120-001-a-0200">of</w>
      <w lemma="the" pos="d" xml:id="B06120-001-a-0210">the</w>
      <w lemma="city" pos="n1" xml:id="B06120-001-a-0220">City</w>
      <w lemma="of" pos="acp" xml:id="B06120-001-a-0230">of</w>
     </hi>
     <w lemma="London" pos="nn1" xml:id="B06120-001-a-0240">London</w>
     <pc xml:id="B06120-001-a-0250">,</pc>
     <hi xml:id="B06120-e90">
      <w lemma="in" pos="acp" xml:id="B06120-001-a-0260">in</w>
      <w lemma="common" pos="j" xml:id="B06120-001-a-0270">Common</w>
      <w lemma="council" pos="n1" xml:id="B06120-001-a-0280">Council</w>
      <w lemma="assemble" pos="vvn" xml:id="B06120-001-a-0290">Assembled</w>
      <pc unit="sentence" xml:id="B06120-001-a-0300">.</pc>
     </hi>
    </head>
    <opener xml:id="B06120-e100">
     <w lemma="showeth" pos="n2" reg="SHOWETHS" xml:id="B06120-001-a-0310">SHEWETH</w>
     <pc xml:id="B06120-001-a-0320">,</pc>
    </opener>
    <p xml:id="B06120-e110">
     <w lemma="that" pos="cs" rend="decorinit" xml:id="B06120-001-a-0330">THat</w>
     <w lemma="we" pos="pns" xml:id="B06120-001-a-0340">we</w>
     <w lemma="your" pos="po" xml:id="B06120-001-a-0350">Your</w>
     <w lemma="most" pos="avs-d" xml:id="B06120-001-a-0360">most</w>
     <w lemma="loyal" pos="j" xml:id="B06120-001-a-0370">Loyal</w>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-0380">and</w>
     <w lemma="dutiful" pos="j" reg="dutiful" xml:id="B06120-001-a-0390">Duetiful</w>
     <w lemma="subject" pos="n2" xml:id="B06120-001-a-0400">Subjects</w>
     <pc xml:id="B06120-001-a-0410">,</pc>
     <w lemma="have" pos="vvg" xml:id="B06120-001-a-0420">having</w>
     <w lemma="with" pos="acp" xml:id="B06120-001-a-0430">with</w>
     <w lemma="astonishment" pos="n1" xml:id="B06120-001-a-0440">Astonishment</w>
     <w lemma="receive" pos="vvd" xml:id="B06120-001-a-0450">received</w>
     <w lemma="the" pos="d" xml:id="B06120-001-a-0460">the</w>
     <w lemma="discovery" pos="n1" xml:id="B06120-001-a-0470">Discovery</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-0480">of</w>
     <w lemma="a" pos="d" xml:id="B06120-001-a-0490">a</w>
     <w lemma="most" pos="avs-d" xml:id="B06120-001-a-0500">most</w>
     <w lemma="traitorous" pos="j" reg="Traitorous" xml:id="B06120-001-a-0510">Traiterous</w>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-0520">and</w>
     <w lemma="horrid" pos="j" xml:id="B06120-001-a-0530">Horrid</w>
     <w lemma="conspiracy" pos="n1" xml:id="B06120-001-a-0540">Conspiracy</w>
     <pc xml:id="B06120-001-a-0550">,</pc>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-0560">of</w>
     <w lemma="divers" pos="j" xml:id="B06120-001-a-0570">divers</w>
     <w lemma="ill" pos="av-j" xml:id="B06120-001-a-0580">Ill</w>
     <w lemma="affect" pos="vvn" xml:id="B06120-001-a-0590">affected</w>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-0600">and</w>
     <w lemma="desperate" pos="j" xml:id="B06120-001-a-0610">Desperate</w>
     <w lemma="person" pos="n2" xml:id="B06120-001-a-0620">Persons</w>
     <pc xml:id="B06120-001-a-0630">,</pc>
     <w lemma="to" pos="prt" xml:id="B06120-001-a-0640">to</w>
     <w lemma="compass" pos="vvi" xml:id="B06120-001-a-0650">compass</w>
     <w lemma="the" pos="d" xml:id="B06120-001-a-0660">the</w>
     <w lemma="death" pos="n1" xml:id="B06120-001-a-0670">Death</w>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-0680">and</w>
     <w lemma="destruction" pos="n1" xml:id="B06120-001-a-0690">Destruction</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-0700">of</w>
     <w lemma="your" pos="po" xml:id="B06120-001-a-0710">Your</w>
     <w lemma="royal" pos="j" xml:id="B06120-001-a-0720">Royal</w>
     <w lemma="person" pos="n1" xml:id="B06120-001-a-0730">Person</w>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-0740">and</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-0750">of</w>
     <w lemma="your" pos="po" xml:id="B06120-001-a-0760">Your</w>
     <w lemma="dear" pos="js" xml:id="B06120-001-a-0770">Dearest</w>
     <w lemma="brother" pos="n1" xml:id="B06120-001-a-0780">Brother</w>
     <w lemma="James" pos="nn1" rend="hi" xml:id="B06120-001-a-0790">James</w>
     <w lemma="duke" pos="n1" xml:id="B06120-001-a-0800">Duke</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-0810">of</w>
     <hi xml:id="B06120-e130">
      <w lemma="York" pos="nn1" xml:id="B06120-001-a-0820">York</w>
      <pc xml:id="B06120-001-a-0830">;</pc>
     </hi>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-0840">and</w>
     <w lemma="that" pos="cs" xml:id="B06120-001-a-0850">that</w>
     <w lemma="to" pos="prt" xml:id="B06120-001-a-0860">to</w>
     <w lemma="effect" pos="vvi" xml:id="B06120-001-a-0870">effect</w>
     <w lemma="the" pos="d" xml:id="B06120-001-a-0880">the</w>
     <w lemma="same" pos="d" xml:id="B06120-001-a-0890">same</w>
     <pc xml:id="B06120-001-a-0900">,</pc>
     <w lemma="they" pos="pns" xml:id="B06120-001-a-0910">they</w>
     <w lemma="have" pos="vvb" xml:id="B06120-001-a-0920">have</w>
     <w lemma="hold" pos="vvn" xml:id="B06120-001-a-0930">held</w>
     <w lemma="several" pos="j" xml:id="B06120-001-a-0940">several</w>
     <w lemma="treasonable" pos="j" xml:id="B06120-001-a-0950">Treasonable</w>
     <w lemma="consultation" pos="n2" xml:id="B06120-001-a-0960">Consultations</w>
     <pc xml:id="B06120-001-a-0970">,</pc>
     <w lemma="to" pos="prt" xml:id="B06120-001-a-0980">to</w>
     <w lemma="levy" pos="vvi" xml:id="B06120-001-a-0990">Levy</w>
     <w lemma="man" pos="n2" xml:id="B06120-001-a-1000">Men</w>
     <pc xml:id="B06120-001-a-1010">,</pc>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-1020">and</w>
     <w lemma="to" pos="prt" xml:id="B06120-001-a-1030">to</w>
     <w lemma="make" pos="vvi" xml:id="B06120-001-a-1040">make</w>
     <w lemma="a" pos="d" xml:id="B06120-001-a-1050">an</w>
     <w lemma="insurrection" pos="n1" xml:id="B06120-001-a-1060">Insurrection</w>
     <pc xml:id="B06120-001-a-1070">,</pc>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-1080">and</w>
     <w lemma="make" pos="vvd" xml:id="B06120-001-a-1090">made</w>
     <w lemma="great" pos="j" xml:id="B06120-001-a-1100">great</w>
     <w lemma="provision" pos="n1" xml:id="B06120-001-a-1110">Provision</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-1120">of</w>
     <w lemma="arm" pos="n2" xml:id="B06120-001-a-1130">Arms</w>
     <pc xml:id="B06120-001-a-1140">:</pc>
     <w lemma="a" pos="d" xml:id="B06120-001-a-1150">A</w>
     <w lemma="design" pos="n1" xml:id="B06120-001-a-1160">Design</w>
     <w lemma="notorious" pos="av-j" xml:id="B06120-001-a-1170">notoriously</w>
     <w lemma="tend" pos="vvg" xml:id="B06120-001-a-1180">tending</w>
     <w lemma="to" pos="acp" xml:id="B06120-001-a-1190">to</w>
     <w lemma="the" pos="d" xml:id="B06120-001-a-1200">the</w>
     <w lemma="present" pos="j" xml:id="B06120-001-a-1210">present</w>
     <w lemma="destruction" pos="n1" xml:id="B06120-001-a-1220">Destruction</w>
     <pc xml:id="B06120-001-a-1230">,</pc>
     <w lemma="not" pos="xx" xml:id="B06120-001-a-1240">not</w>
     <w lemma="only" pos="av-j" xml:id="B06120-001-a-1250">only</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-1260">of</w>
     <w lemma="your" pos="po" xml:id="B06120-001-a-1270">Your</w>
     <w lemma="best" pos="js" xml:id="B06120-001-a-1280">best</w>
     <w lemma="subject" pos="n2" xml:id="B06120-001-a-1290">Subjects</w>
     <pc xml:id="B06120-001-a-1300">,</pc>
     <w lemma="but" pos="acp" xml:id="B06120-001-a-1310">but</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-1320">of</w>
     <w lemma="the" pos="d" xml:id="B06120-001-a-1330">the</w>
     <w lemma="sacred" pos="j" xml:id="B06120-001-a-1340">Sacred</w>
     <w lemma="person" pos="n1" xml:id="B06120-001-a-1350">Person</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-1360">of</w>
     <w lemma="your" pos="po" xml:id="B06120-001-a-1370">Your</w>
     <w lemma="majesty" pos="n1" xml:id="B06120-001-a-1380">Majesty</w>
     <pc xml:id="B06120-001-a-1390">,</pc>
     <w lemma="the" pos="d" xml:id="B06120-001-a-1400">the</w>
     <w lemma="best" pos="js" xml:id="B06120-001-a-1410">best</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-1420">of</w>
     <w lemma="prince" pos="n2" xml:id="B06120-001-a-1430">Princes</w>
     <pc xml:id="B06120-001-a-1440">,</pc>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-1450">and</w>
     <w lemma="to" pos="prt" xml:id="B06120-001-a-1460">to</w>
     <w lemma="involve" pos="vvi" xml:id="B06120-001-a-1470">Involve</w>
     <w lemma="this" pos="d" xml:id="B06120-001-a-1480">this</w>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-1490">and</w>
     <w lemma="the" pos="d" xml:id="B06120-001-a-1500">the</w>
     <w lemma="future" pos="j" xml:id="B06120-001-a-1510">future</w>
     <w lemma="generation" pos="n1" xml:id="B06120-001-a-1520">Generation</w>
     <w lemma="in" pos="acp" xml:id="B06120-001-a-1530">in</w>
     <w lemma="confusion" pos="n1" xml:id="B06120-001-a-1540">Confusion</w>
     <pc xml:id="B06120-001-a-1550">,</pc>
     <w lemma="blood" pos="n1" reg="Blood" xml:id="B06120-001-a-1560">Bloud</w>
     <pc xml:id="B06120-001-a-1570">,</pc>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-1580">and</w>
     <w lemma="misery" pos="n1" xml:id="B06120-001-a-1590">Misery</w>
     <pc xml:id="B06120-001-a-1600">;</pc>
     <w lemma="carry" pos="vvn" xml:id="B06120-001-a-1610">carried</w>
     <w lemma="on" pos="acp" xml:id="B06120-001-a-1620">on</w>
     <pc xml:id="B06120-001-a-1630">,</pc>
     <w lemma="notwithstanding" pos="acp" xml:id="B06120-001-a-1640">notwithstanding</w>
     <w lemma="their" pos="po" xml:id="B06120-001-a-1650">their</w>
     <w lemma="specious" pos="j" xml:id="B06120-001-a-1660">specious</w>
     <w lemma="pretence" pos="n2" xml:id="B06120-001-a-1670">Pretences</w>
     <pc xml:id="B06120-001-a-1680">,</pc>
     <w lemma="by" pos="acp" xml:id="B06120-001-a-1690">by</w>
     <w lemma="know" pos="j-vn" xml:id="B06120-001-a-1700">known</w>
     <w lemma="dissent" pos="vvg" xml:id="B06120-001-a-1710">Dissenting</w>
     <w lemma="conventicler" pos="n2" xml:id="B06120-001-a-1720">Conventiclers</w>
     <pc xml:id="B06120-001-a-1730">,</pc>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-1740">and</w>
     <w lemma="atheistical" pos="j" xml:id="B06120-001-a-1750">Atheistical</w>
     <w lemma="person" pos="n2" xml:id="B06120-001-a-1760">Persons</w>
     <pc unit="sentence" xml:id="B06120-001-a-1770">.</pc>
    </p>
    <p xml:id="B06120-e140">
     <hi xml:id="B06120-e150">
      <w lemma="and" pos="cc" xml:id="B06120-001-a-1780">And</w>
      <w lemma="have" pos="vvg" xml:id="B06120-001-a-1790">having</w>
      <w lemma="in" pos="acp" xml:id="B06120-001-a-1800">in</w>
      <w lemma="the" pos="d" xml:id="B06120-001-a-1810">the</w>
      <w lemma="first" pos="ord" xml:id="B06120-001-a-1820">first</w>
      <w lemma="place" pos="n1" xml:id="B06120-001-a-1830">place</w>
      <w lemma="offer" pos="vvd" xml:id="B06120-001-a-1840">offered</w>
      <w lemma="up" pos="acp" xml:id="B06120-001-a-1850">up</w>
      <w lemma="our" pos="po" xml:id="B06120-001-a-1860">our</w>
      <w lemma="solemn" pos="j" xml:id="B06120-001-a-1870">solemn</w>
      <w lemma="thanks" pos="n2" xml:id="B06120-001-a-1880">Thanks</w>
      <w lemma="to" pos="acp" xml:id="B06120-001-a-1890">to</w>
      <w lemma="almighty" pos="j" xml:id="B06120-001-a-1900">Almighty</w>
      <w lemma="God" pos="nn1" xml:id="B06120-001-a-1910">God</w>
      <pc xml:id="B06120-001-a-1920">,</pc>
      <w lemma="for" pos="acp" xml:id="B06120-001-a-1930">for</w>
      <w lemma="his" pos="po" xml:id="B06120-001-a-1940">His</w>
      <w lemma="watchful" pos="j" xml:id="B06120-001-a-1950">watchful</w>
      <w lemma="providence" pos="n1" xml:id="B06120-001-a-1960">Providence</w>
      <w lemma="in" pos="acp" xml:id="B06120-001-a-1970">in</w>
      <w lemma="bring" pos="vvg" xml:id="B06120-001-a-1980">bringing</w>
      <w lemma="to" pos="prt" xml:id="B06120-001-a-1990">to</w>
      <w lemma="light" pos="vvi" xml:id="B06120-001-a-2000">Light</w>
      <w lemma="this" pos="d" xml:id="B06120-001-a-2010">this</w>
      <w lemma="impious" pos="j" xml:id="B06120-001-a-2020">Impious</w>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-2030">and</w>
      <w lemma="execrable" pos="j" xml:id="B06120-001-a-2040">Execrable</w>
      <w lemma="machination" pos="n1" xml:id="B06120-001-a-2050">Machination</w>
      <pc xml:id="B06120-001-a-2060">,</pc>
     </hi>
    </p>
    <p xml:id="B06120-e160">
     <hi xml:id="B06120-e170">
      <w lemma="we" pos="pns" xml:id="B06120-001-a-2070">We</w>
      <w lemma="do" pos="vvb" xml:id="B06120-001-a-2080">do</w>
      <w lemma="in" pos="acp" xml:id="B06120-001-a-2090">in</w>
      <w lemma="the" pos="d" xml:id="B06120-001-a-2100">the</w>
      <w lemma="next" pos="ord" xml:id="B06120-001-a-2110">next</w>
      <w lemma="place" pos="n1" xml:id="B06120-001-a-2120">place</w>
      <w lemma="humble" pos="av-j" xml:id="B06120-001-a-2130">humbly</w>
      <w lemma="offer" pos="vvi" xml:id="B06120-001-a-2140">offer</w>
      <w lemma="to" pos="acp" xml:id="B06120-001-a-2150">to</w>
      <w lemma="your" pos="po" xml:id="B06120-001-a-2160">Your</w>
      <w lemma="majesty" pos="n1" xml:id="B06120-001-a-2170">Majesty</w>
      <w lemma="the" pos="d" xml:id="B06120-001-a-2180">the</w>
      <w lemma="deep" pos="j" xml:id="B06120-001-a-2190">deep</w>
      <w lemma="resentment" pos="n2" xml:id="B06120-001-a-2200">Resentments</w>
      <w lemma="of" pos="acp" xml:id="B06120-001-a-2210">of</w>
      <w lemma="our" pos="po" xml:id="B06120-001-a-2220">our</w>
      <w lemma="loyal" pos="j" xml:id="B06120-001-a-2230">Loyal</w>
      <w lemma="heart" pos="n2" xml:id="B06120-001-a-2240">Hearts</w>
      <w lemma="concern" pos="vvg" xml:id="B06120-001-a-2250">concerning</w>
      <w lemma="the" pos="d" xml:id="B06120-001-a-2260">the</w>
      <w lemma="same" pos="d" xml:id="B06120-001-a-2270">same</w>
      <pc xml:id="B06120-001-a-2280">,</pc>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-2290">and</w>
      <w lemma="beg" pos="vvi" xml:id="B06120-001-a-2300">beg</w>
      <w lemma="your" pos="po" xml:id="B06120-001-a-2310">Your</w>
      <w lemma="majesty" pos="n1" xml:id="B06120-001-a-2320">Majesty</w>
      <w lemma="to" pos="acp" xml:id="B06120-001-a-2330">to</w>
      <w lemma="rest" pos="n1" xml:id="B06120-001-a-2340">Rest</w>
      <w lemma="full" pos="av-j" xml:id="B06120-001-a-2350">fully</w>
      <w lemma="assure" pos="vvn" xml:id="B06120-001-a-2360">assured</w>
      <pc xml:id="B06120-001-a-2370">,</pc>
      <w lemma="that" pos="cs" xml:id="B06120-001-a-2380">That</w>
      <w lemma="as" pos="acp" xml:id="B06120-001-a-2390">as</w>
      <w lemma="no" pos="dx" xml:id="B06120-001-a-2400">no</w>
      <w lemma="interest" pos="n1" xml:id="B06120-001-a-2410">Interest</w>
      <w lemma="in" pos="acp" xml:id="B06120-001-a-2420">in</w>
      <w lemma="this" pos="d" xml:id="B06120-001-a-2430">this</w>
      <w lemma="world" pos="n1" xml:id="B06120-001-a-2440">World</w>
      <w lemma="be" pos="vvz" xml:id="B06120-001-a-2450">is</w>
      <w lemma="valuable" pos="j" xml:id="B06120-001-a-2460">valuable</w>
      <w lemma="to" pos="acp" xml:id="B06120-001-a-2470">to</w>
      <w lemma="we" pos="pno" xml:id="B06120-001-a-2480">us</w>
      <w lemma="in" pos="acp" xml:id="B06120-001-a-2490">in</w>
      <w lemma="comparison" pos="n1" xml:id="B06120-001-a-2500">comparison</w>
      <w lemma="of" pos="acp" xml:id="B06120-001-a-2510">of</w>
      <w lemma="your" pos="po" xml:id="B06120-001-a-2520">Your</w>
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="B06120-001-a-2530">Majesties</w>
      <w lemma="service" pos="n1" xml:id="B06120-001-a-2540">Service</w>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-2550">and</w>
      <w lemma="safety" pos="n1" xml:id="B06120-001-a-2560">Safety</w>
      <pc xml:id="B06120-001-a-2570">;</pc>
      <w lemma="so" pos="av" xml:id="B06120-001-a-2580">so</w>
      <w lemma="we" pos="pns" xml:id="B06120-001-a-2590">we</w>
      <w lemma="be" pos="vvb" xml:id="B06120-001-a-2600">are</w>
      <w lemma="determine" pos="vvn" xml:id="B06120-001-a-2610">Determined</w>
      <w lemma="ready" pos="av-j" xml:id="B06120-001-a-2620">readily</w>
      <w lemma="to" pos="prt" xml:id="B06120-001-a-2630">to</w>
      <w lemma="expose" pos="vvb" xml:id="B06120-001-a-2640">Expose</w>
      <w lemma="our" pos="po" xml:id="B06120-001-a-2650">our</w>
      <w lemma="life" pos="n2" xml:id="B06120-001-a-2660">Lives</w>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-2670">and</w>
      <w lemma="fortune" pos="n2" xml:id="B06120-001-a-2680">Fortunes</w>
      <w lemma="in" pos="acp" xml:id="B06120-001-a-2690">in</w>
      <w lemma="defence" pos="n1" xml:id="B06120-001-a-2700">Defence</w>
      <w lemma="of" pos="acp" xml:id="B06120-001-a-2710">of</w>
      <w lemma="your" pos="po" xml:id="B06120-001-a-2720">Your</w>
      <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="B06120-001-a-2730">Majesties</w>
      <w lemma="person" pos="n1" xml:id="B06120-001-a-2740">Person</w>
      <pc xml:id="B06120-001-a-2750">,</pc>
      <w lemma="your" pos="po" xml:id="B06120-001-a-2760">Your</w>
      <w lemma="heir" pos="n2" xml:id="B06120-001-a-2770">Heirs</w>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-2780">and</w>
      <w lemma="successor" pos="n2" xml:id="B06120-001-a-2790">Successors</w>
      <pc xml:id="B06120-001-a-2800">,</pc>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-2810">and</w>
      <w lemma="your" pos="po" xml:id="B06120-001-a-2820">Your</w>
      <w lemma="government" pos="n1" xml:id="B06120-001-a-2830">Government</w>
      <w lemma="establish" pos="vvn" xml:id="B06120-001-a-2840">Established</w>
      <w lemma="in" pos="acp" xml:id="B06120-001-a-2850">in</w>
      <w lemma="church" pos="n1" xml:id="B06120-001-a-2860">Church</w>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-2870">and</w>
      <w lemma="state" pos="n1" xml:id="B06120-001-a-2880">State</w>
      <pc xml:id="B06120-001-a-2890">,</pc>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-2900">and</w>
      <w lemma="particular" pos="av-j" xml:id="B06120-001-a-2910">particularly</w>
      <pc xml:id="B06120-001-a-2920">,</pc>
      <w lemma="for" pos="acp" xml:id="B06120-001-a-2930">for</w>
      <w lemma="discover" pos="vvg" xml:id="B06120-001-a-2940">Discovering</w>
      <pc xml:id="B06120-001-a-2950">,</pc>
      <w lemma="defeat" pos="vvg" xml:id="B06120-001-a-2960">Defeating</w>
      <pc xml:id="B06120-001-a-2970">,</pc>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-2980">and</w>
      <w lemma="destroy" pos="vvg" xml:id="B06120-001-a-2990">Destroying</w>
      <w lemma="all" pos="d" xml:id="B06120-001-a-3000">all</w>
      <w lemma="such" pos="d" xml:id="B06120-001-a-3010">such</w>
      <w lemma="conspiracy" pos="n2" xml:id="B06120-001-a-3020">Conspiracies</w>
      <pc xml:id="B06120-001-a-3030">,</pc>
      <w lemma="association" pos="n2" xml:id="B06120-001-a-3040">Associations</w>
      <pc xml:id="B06120-001-a-3050">,</pc>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-3060">and</w>
      <w lemma="attempt" pos="vvz" xml:id="B06120-001-a-3070">Attempts</w>
      <w lemma="whatsoever" pos="crq" xml:id="B06120-001-a-3080">whatsoever</w>
      <pc unit="sentence" xml:id="B06120-001-a-3090">.</pc>
     </hi>
    </p>
    <p xml:id="B06120-e180">
     <hi xml:id="B06120-e190">
      <w lemma="all" pos="av-d" xml:id="B06120-001-a-3100">All</w>
      <w lemma="which" pos="crq" xml:id="B06120-001-a-3110">which</w>
      <w lemma="resolution" pos="n2" xml:id="B06120-001-a-3120">Resolutions</w>
      <w lemma="be" pos="vvb" xml:id="B06120-001-a-3130">are</w>
      <w lemma="accompany" pos="vvn" xml:id="B06120-001-a-3140">accompanied</w>
      <w lemma="with" pos="acp" xml:id="B06120-001-a-3150">with</w>
      <w lemma="our" pos="po" xml:id="B06120-001-a-3160">our</w>
      <w lemma="daily" pos="j" xml:id="B06120-001-a-3170">Daily</w>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-3180">and</w>
      <w lemma="fervent" pos="j" xml:id="B06120-001-a-3190">Fervent</w>
      <w lemma="prayer" pos="n2" xml:id="B06120-001-a-3200">Prayers</w>
      <pc xml:id="B06120-001-a-3210">,</pc>
      <w lemma="that" pos="cs" xml:id="B06120-001-a-3220">That</w>
      <w lemma="your" pos="po" xml:id="B06120-001-a-3230">Your</w>
      <w lemma="majesty" pos="n1" xml:id="B06120-001-a-3240">Majesty</w>
      <w lemma="may" pos="vmb" xml:id="B06120-001-a-3250">may</w>
      <w lemma="vanquish" pos="vvb" xml:id="B06120-001-a-3260">Vanquish</w>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-3270">and</w>
      <w lemma="overcome" pos="vvi" xml:id="B06120-001-a-3280">overcome</w>
      <w lemma="all" pos="d" xml:id="B06120-001-a-3290">all</w>
      <w lemma="your" pos="po" xml:id="B06120-001-a-3300">Your</w>
      <w lemma="enemy" pos="n2" xml:id="B06120-001-a-3310">Enemies</w>
      <pc xml:id="B06120-001-a-3320">;</pc>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-3330">And</w>
      <w lemma="that" pos="cs" xml:id="B06120-001-a-3340">that</w>
      <w lemma="the" pos="d" xml:id="B06120-001-a-3350">the</w>
      <w lemma="year" pos="n2" xml:id="B06120-001-a-3360">years</w>
      <w lemma="of" pos="acp" xml:id="B06120-001-a-3370">of</w>
      <w lemma="your" pos="po" xml:id="B06120-001-a-3380">Your</w>
      <w lemma="happy" pos="j" xml:id="B06120-001-a-3390">Happy</w>
      <w lemma="reign" pos="n1" xml:id="B06120-001-a-3400">Reign</w>
      <w lemma="over" pos="acp" xml:id="B06120-001-a-3410">over</w>
      <w lemma="we" pos="pno" xml:id="B06120-001-a-3420">us</w>
      <pc xml:id="B06120-001-a-3430">,</pc>
      <w lemma="may" pos="vmb" xml:id="B06120-001-a-3440">may</w>
      <w lemma="be" pos="vvi" xml:id="B06120-001-a-3450">be</w>
      <w lemma="many" pos="d" xml:id="B06120-001-a-3460">many</w>
      <w lemma="and" pos="cc" xml:id="B06120-001-a-3470">and</w>
      <w lemma="prosperous" pos="j" xml:id="B06120-001-a-3480">Prosperous</w>
      <pc unit="sentence" xml:id="B06120-001-a-3490">.</pc>
     </hi>
    </p>
   </div>
  </body>
  <back xml:id="B06120-e200">
   <div type="license" xml:id="B06120-e210">
    <opener xml:id="B06120-e220">
     <dateline xml:id="B06120-e230">
      <date xml:id="B06120-e240">
       <w lemma="2" pos="crd" xml:id="B06120-001-a-3500">2</w>
       <w lemma="July" pos="nn1" rend="hi" xml:id="B06120-001-a-3510">July</w>
       <w lemma="1683." pos="crd" xml:id="B06120-001-a-3520">1683.</w>
       <pc unit="sentence" xml:id="B06120-001-a-3530"/>
      </date>
     </dateline>
    </opener>
    <p xml:id="B06120-e260">
     <w lemma="it" pos="pn" xml:id="B06120-001-a-3540">It</w>
     <w lemma="be" pos="vvz" xml:id="B06120-001-a-3550">is</w>
     <w lemma="his" pos="po" xml:id="B06120-001-a-3560">His</w>
     <w lemma="majesty" pos="ng1" reg="Majesty's" xml:id="B06120-001-a-3570">Majesties</w>
     <w lemma="pleasure" pos="n1" xml:id="B06120-001-a-3580">Pleasure</w>
     <w lemma="that" pos="cs" xml:id="B06120-001-a-3590">that</w>
     <w lemma="this" pos="d" xml:id="B06120-001-a-3600">this</w>
     <w lemma="humble" pos="j" xml:id="B06120-001-a-3610">Humble</w>
     <w lemma="address" pos="n1" xml:id="B06120-001-a-3620">Address</w>
     <w lemma="be" pos="vvi" xml:id="B06120-001-a-3630">be</w>
     <w lemma="forthwith" pos="av" xml:id="B06120-001-a-3640">forthwith</w>
     <w lemma="print" pos="vvn" xml:id="B06120-001-a-3650">Printed</w>
     <w lemma="and" pos="cc" xml:id="B06120-001-a-3660">and</w>
     <w lemma="publish" pos="vvn" xml:id="B06120-001-a-3670">Published</w>
     <pc unit="sentence" xml:id="B06120-001-a-3680">.</pc>
    </p>
    <closer xml:id="B06120-e270">
     <signed xml:id="B06120-e280">
      <w lemma="l." pos="ab" xml:id="B06120-001-a-3690">L.</w>
      <w lemma="Jenkins" pos="nn1" xml:id="B06120-001-a-3700">JENKINS</w>
      <pc unit="sentence" xml:id="B06120-001-a-3710">.</pc>
     </signed>
    </closer>
   </div>
   <div type="colophon" xml:id="B06120-e290">
    <p xml:id="B06120-e300">
     <hi xml:id="B06120-e310">
      <w lemma="Edinburgh" pos="nn1" reg="EDINBURGH" xml:id="B06120-001-a-3720">EDINBVRGH</w>
      <pc xml:id="B06120-001-a-3730">,</pc>
     </hi>
     <w lemma="reprint" pos="vvn" reg="reprinted" xml:id="B06120-001-a-3740">Re-printed</w>
     <pc xml:id="B06120-001-a-3750">,</pc>
     <w lemma="in" pos="acp" xml:id="B06120-001-a-3760">in</w>
     <w lemma="the" pos="d" xml:id="B06120-001-a-3770">the</w>
     <w lemma="year" pos="n1" xml:id="B06120-001-a-3780">Year</w>
     <w lemma="of" pos="acp" xml:id="B06120-001-a-3790">of</w>
     <w lemma="GOD" pos="nn1" xml:id="B06120-001-a-3800">GOD</w>
     <pc xml:id="B06120-001-a-3810">,</pc>
     <w lemma="1683." pos="crd" xml:id="B06120-001-a-3820">1683.</w>
     <pc unit="sentence" xml:id="B06120-001-a-3830"/>
    </p>
   </div>
  </back>
 </text>
</TEI>
